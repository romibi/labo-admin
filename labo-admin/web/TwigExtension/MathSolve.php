<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin\Twig;
use jlawrence\eos\Parser;

class MathSolve extends \Twig_Extension
{

    public function getFunctions() 
    {
        return array(
            new \Twig_SimpleFunction('solve', array($this, 'solve'), array('is_safe' => array('html'))),
        );
    }

    public function solve($formula, $values) {
        try{
            return Parser::solveIf($formula, $values);
        } catch(\Exception $e) {
            return preg_replace('/^|\ *in\W*$/', '', $e->getMessage());
        }
    }

    public function getName()
    {
        return 'MathSolve';
    }
}