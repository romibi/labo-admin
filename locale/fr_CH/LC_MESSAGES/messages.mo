��    K      t  e   �      `     a     g     k     o     u     }     �  	   �  
   �     �     �     �     �     �  	   �     �     �               !     *     :  P   @  +   �     �  	   �     �     �     �     �               ,     3     :     A     T     n     z     �     �     �     �     �     �     �     �     �     �     �     �      	  
   	     	     	     +	     =	     E	     L	     Q	     a	     p	     u	     �	     �	     �	     �	  �   �	     �
     �
     �
     �
     �
     �
  �  �
     F     L     P  
   T     _     f     s     z     �     �     �     �     �     �               !     &     <     R     ^     r  c   x     �  
   �     �          (     9     Y     w     �  	   �     �     �     �     �     �                9     L     T     X     g     {          �     �     �     �     �     �     �     �     �  
   �                    3     D     G     b     ~     �     �  �   �     �     �     �     �     �     �     0   
       J                        )      9          5          F      ;   I   H   4       .       7                 @   !   '   ?   #   ,                    +          A      %       <             C   G   6   2   >   (   D      K                         "          $              B   3   1          -          &       /      8              E   =            *   	   :    %s-%s <%s >%s Abort Actions Are you sure? Back Birthdate Birthdate: Close Comment Confirm Delete Create new patient Create new sample Datasheet Datasheet PDF Date Date of sample Delete sample Diabetes Disable Blocker Div 0 Do you really want to delete the following Sample with all it's saved test data? Doctor Who? / Which address should be used? Done Edit date Edit patient Edit regardless Edit sample Edit sample date Edit sample results Edit sample tests French Gender German Go to patient page Has the patient diabetes? Labor tests Last Patient List of samples Mark as seen Marked Name Name of patient No dates No: Not done Not done tests Not seen Other Results PDF Patient Patient ID Patient: Points analyse Points supplement Results Sample Save Save and Submit Search patient Seen Select doctor Show datasheet PDF Show sample Tasks Tests with no dates This labo-admin instance is only for demonstration. It might be in an unfinished state and probably will differ from the next release!
Please keep in mind, that any entered information can be viewed publicly! Total Unseen results Warning Yes ♀(f) ♂(m) Project-Id-Version: labo-admin
Report-Msgid-Bugs-To: info@romibi.ch
POT-Creation-Date: 2018-04-21 12:26+0200
PO-Revision-Date: 2018-04-21 12:29+0200
Last-Translator: Rolf Michael Bislin <info@romibi.ch>
Language-Team: 
Language: fr_CH
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n > 1);
 %s-%s <%s >%s Abandonner Action Assurément? Retour Date de naissance Date de naissance: Fermer Commentaire Effacer définitivement Saisir un nouveau patient Saisir nouveau prélèvement Feuille labo Feuille labo (PDF) Date Date du prélèvement Effacer prélèvement Diabétique Désactiver blocage Div 0 Est-ce que vous voulez vraiment effacer le test suivant y inclus tous les commandes de test liées? Médecin émetteur? Été fait Changer date Changer les données du patient Pourtant changer Éditer tests du prélèvements Changer date du prélèvement Changer résultats des tests Éditer tests du prélèvements Français Sexe Allemand Vers la page du patient Patient diabétique? Examens de laboratoire Dernier patient Liste des prélèvements Marquer comme "vu" Marqué Nom Nom du patient Date labo à donner No: À faire Tests à faire Pas vus Autres resultats PDF Patient Code du patient Patient: Points analyse Points supplement Résultats Prélèvements Sauvegarder Sauvegarder et envoyer Chercher patient Vu Changer médecin émetteur Afficher Feuille labo (PDF) Afficher prélèvement Action Date labo à donner Cette labo-admin Demo est seulement pour demonstration. C'est pas programme fini et veut etre different comme le prochaine officielle version!
Remembre: Toutes les informations peut étre visible pour tousse! Total Résultats pas vus Avertissement! Oui ♀(f) ♂(m) 