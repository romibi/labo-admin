<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'MenuController.php';
class WebController {
	protected $menuController = null;
	protected $footerController = null;
	public function __construct() {
		$this->menuController = new MenuController();
		$this->footerController = new MenuController();
	}

	public function menu() {
		return $this->menuController;
	}

	public function footer() {
		return $this->footerController;
	}

	public function getVersionString($short=false) {
		$release = include __DIR__.'/../version.php';
		if($short) {
			$release = explode("-", $release)[0];
		}
		$showGitVersion = LaboAdmin::getInstance()->config()->get('layout.showGitVersion', 'false')=='true';

		$version = $release;
		$config = LaboAdmin::getInstance()->getBaseConfig();
		if(!isset($config['custom']['gitVersionPath'])) {
			return $version;
		}
		$gitfilepath = $config['custom']['gitVersionPath'];
		
		if($showGitVersion && !$short) {
			try {
				/* get git-version */
				if(file_exists($gitfilepath)) {
					$gitfile = fopen($gitfilepath, "r");
					$gitversion = fread($gitfile,filesize($gitfilepath));

					$version = $release.' ('.substr($gitversion, 0, 7).')';
				}
			} catch (Exception $e) {
			} finally {
				@fclose($gitfile);
			}
		}
		return htmlspecialchars($version); //one never knows
	}
}