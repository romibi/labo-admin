<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
// Based on https://www.sitepoint.com/easy-multi-language-twig-apps-with-gettext/
require __DIR__.'/../../vendor/autoload.php';
$config = require __DIR__.'/../../configs/config.php';
require __DIR__.'/../web/TwigExtension/MathSolve.php';

$tplDir = realpath(__DIR__.'/../../templates');
$tmpDir = __DIR__.'/../../tmp/cache/';

$loader = new Twig_Loader_Filesystem(realpath($config['slim']['templates.path']));

// force auto-reload to always have the latest version of the template
$twig = new Twig_Environment(
    $loader, [
    'cache' => $tmpDir,
    'auto_reload' => true,
]
);
$twig->addExtension(new Twig_Extensions_Extension_I18n());
$twig->addExtension(new \ch\romibi\labo_admin\Twig\MathSolve());
// configure Twig the way you want

// iterate over all your templates
foreach (new RecursiveIteratorIterator(
             new RecursiveDirectoryIterator(realpath($config['slim']['templates.path'])),
             RecursiveIteratorIterator::LEAVES_ONLY
         ) as $file) {
    // force compilation
    if ($file->isFile()) {
    	$templateName = str_replace(
    					str_replace('\\','/',$tplDir) . '/',
    					'', str_replace('\\','/',$file));
    	$twig->loadTemplate($templateName);
    }
}