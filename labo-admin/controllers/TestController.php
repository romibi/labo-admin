<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractController.php';
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class TestController extends AbstractController {
	protected $entityManager;
	protected $repository;
	protected $sampleRepo;
	protected $testtypeRepo;
	public function __construct($entityManager) {
		$this->entityManager = $entityManager;
		$this->repository = $entityManager->getRepository('ch\romibi\labo_admin\Test');
		$this->sampleRepo = $entityManager->getRepository('ch\romibi\labo_admin\Sample');
		$this->testtypeRepo = $entityManager->getRepository('ch\romibi\labo_admin\TestType');
	}

	public function get($sampleid, $testtype) {
		$sample = $this->sampleRepo->find($sampleid);
		$testtype = $this->testtypeRepo->find($testtype);
		$test = $this->repository->findBy(array('sample'=>$sample,'testtype'=>$testtype))[0];
		return $test;
	}

	private function getId($sampleid, $testtype) {
		$test = $this->get($sampleid, $testtype);
		return $test->getId();
	}

	public function put($sampleid, $testtype, $object) {
		if(!isset($object['sampleId'])) { $object['sampleId']=$sampleid; }
		if(!isset($object['testtypeId'])) { $object['testtypeId']=$testtype; }
		if(strtolower($testtype) != strtolower($object['testtypeId'])) { throw new \Exception(_('Multiple different testtypeIds provided')); }
		
		$newtest = Test::fromArray($object);
		$newtestTestType = $newtest->getTestType();
		$retval = null;
		if($newtestTestType==null) { throw new \Exception(_('Unknown testtypeId provided')); }
		if($newtestTestType->getType()==TestType::TT_CALCULATED) {
			throw new \Exception(_("Trying to persist test of calculated type. This is not allowed!"));
		} else if($newtestTestType->getType()==TestType::TT_GROUP) {
			$retval = array();
			$sample = $newtest->getSample();
			foreach ($newtestTestType->getContains() as $value) {
				$existing = $sample->getTestByTypeId($value);
				if($existing==null)	{
					$retval[] = $this->put($sampleid, $value, array());
				}
			}
			foreach ($newtestTestType->getContainsNot() as $value) {
				$existing = $sample->getTestByTypeId($value);
				if($existing!=null) {
					$this->delete($sampleid, $value);
				}
			}
		} else {
			$this->entityManager->persist($newtest);
			$this->entityManager->flush();
			$retval = $newtest;
		}
		return $retval;
	}

	private function putGroup() {
		
	}

	public function patch($sampleid, $testtype, $object) {
		if(!isset($object['sampleId'])) { $object['sampleId']=$sampleid; }
		if(!isset($object['testtypeId'])) { $object['typeId']=$testtype; }
		if(strtolower($testtype) != strtolower($object['testtypeId'])) { throw new \Exception(_('Multiple different testtypeIds provided')); }
		if(!isset($object['_version'])) { $object['_version']=-1; }

		$testid = $this->getId($sampleid, $testtype);
		try{
			$test = $this->repository->find($testid, LockMode::OPTIMISTIC, $object['_version']);
			$test->updateFromArray($object);
			$this->entityManager->flush();
			return $test;
		} catch(OptimisticLockException $e) {
			$testtypeobj = $this->testtypeRepo->find($testtype);
			throw new EntityChangedException(sprintf(_("Someone already changed sample test \"%s\""), $testtypeobj->getName()));
		}
	}

	public function delete($sampleid, $testtype) {
		$testtypeObj = $this->testtypeRepo->find($testtype);
		// what if type calculated?
		if($testtypeObj->getType()==TestType::TT_GROUP) {
			foreach ($testtypeObj->getContains() as $value) {
				$this->delete($sampleid, $value);
			}
		} else {
			$test = $this->get($sampleid, $testtype);
			$this->entityManager->remove($test);
			$this->entityManager->flush();
		}
		return true;
	}
}
?>