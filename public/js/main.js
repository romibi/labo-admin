// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).

// render() and getTemplate() based on https://stackoverflow.com/a/39065147
function render(templateName, props) {
	var template = getTemplate(templateName);
	return template.map(function(tok, i) { return (i % 2) ? props[tok] : tok; }).join('');
}

function getTemplate(name) {
	return $('script[data-template="'+name+'"]').text().split(/\$\{(.+?)\}/g);
}

function getErrorFromXHR(xhr) {
	var messageobj = xhr.responseJSON;
	if(typeof messageobj == "undefined") {
		messageobj = cleanupJson(xhr.responseText);
	}
	var errorMessage = "Unknown Error";
	if(messageobj.message) {
		errorMessage = messageobj.message;
	}
	if(messageobj.exception) {
		errorMessage += ": "+messageobj.exception[0].message;
	} else if(messageobj.error) {
		errorMessage += ": "+messageobj.error[0].message;
	}
	return errorMessage;
}

function cleanupJson(dirtyJSON) {
	var firstCurl = dirtyJSON.indexOf('{');
	if(firstCurl==-1) firstCurl = Number.MAX_SAFE_INTEGER;
	var firstBrack = dirtyJSON.indexOf('[');
	if(firstBrack==-1) firstBrack = Number.MAX_SAFE_INTEGER;
	var sliceStart = Math.min(firstCurl, firstBrack);
	if(sliceStart>0) {
		dirtyJSON = dirtyJSON.slice(sliceStart);
	}
	var lastCurl = dirtyJSON.lastIndexOf('}');
	var lastBrack = dirtyJSON.lastIndexOf(']');
	var sliceEnd = Math.max(lastCurl, lastBrack)+1;
	if(dirtyJSON.length>sliceEnd) {
		dirtyJSON.slice(0,sliceEnd);
	}
	return JSON.parse(dirtyJSON);
}


$( document ).ready(function() {
	/*
	 * Replace all SVG images with inline SVG
	 * by http://stackoverflow.com/a/11978996
	 */
	jQuery('img.svg').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Replace image with new SVG
			$img.replaceWith($svg);
		}, 'xml');
	});

	$.widget.bridge('uitooltip', $.ui.tooltip);
	$(function() {
		$( document ).uitooltip();
	});
});


String.prototype.nl2br = function() {
	return this.replace(/\n/g, "<br />");
}