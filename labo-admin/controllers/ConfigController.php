<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractController.php';
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class ConfigController extends AbstractController {
	protected $entityManager;
	protected $repository;
	public function __construct($entityManager) {
		$this->entityManager = $entityManager;
		$this->repository = $entityManager->getRepository('ch\romibi\labo_admin\Config');
	}

	public function get($key, $default='') {
		$config = $this->repository->find($key);
		if($config==null) {
			return $default;
		}
		return $config->getValue();
	}

	public function getJSON($key, $default=array()) {
		$config = $this->get($key, '');
		if($config=='') {
			return $default;
		}
		return json_decode($config);
	}

	public function put($key, $object) {
		if(!isset($object['key'])) { $object['key']=$key; }
		if(strtolower($key)!= strtolower($object['key'])) { throw new \Exception(_('Multiple different keys provided')); }
		$newconfig = Config::fromArray($object);
		$this->entityManager->persist($newconfig);
		$this->entityManager->flush();
		return $newconfig;
	}

	public function patch($key, $object) {
		if(!isset($object['key'])) { $object['key']=$key; }
		if(strtolower($key)!= strtolower($object['key'])) { throw new \Exception(_('Multiple different keys provided')); }
		if(!isset($object['_version'])) { $object['_version']=-1; }
		try {
			$config = $this->repository->find($id, LockMode::OPTIMISTIC, $object['_version']);
			$config->updateFromArray($object);
			$this->entityManager->flush();
			return $config;
		} catch(OptimisticLockException $e) {
			throw new EntityChangedException(_("Someone already changed this config setting"));
		}
	}

	public function delete($key) {
		if(!isset($object['key'])) { $object['key']=$key; }
		if(strtolower($key)!= strtolower($object['key'])) { throw new \Exception(_('Multiple different keys provided')); }
		$config = $this->repository->find($key);
		$this->entityManager->remove($config);
		$this->entityManager->flush();
		return true;
	}
}
?>