<?php
return array(
	'slim' => array(
        'settings' => [
        'displayErrorDetails' => true,
        ],
		'templates.path' => __DIR__ . '/../templates',
		'log.level' => 4,
		'log.enabled' => true,
		'log.writer' => new Slim\Logger\DateTimeFileWriter(
			array(
				'path' => __DIR__ . '/../logs',
				'name_format' => 'y-m-d'
			)
		)
	),
	'twig' => array(
            //'charset' => 'utf-8',
            //'cache' => __DIR__ . '/../templates/cache',
            'cache' => false,
            //'auto_reload' => false,
            //'strict_variables' => true,
            //'autoescape' => true,
            'debug' => true,
    ),
    'db' => array(
        'driver'   => 'pdo_mysql',
        'user'     => 'DBUSER',
        'password' => 'DBPASS',
        'dbname'   => 'DBNAME',
        'host'     => 'localhost',
        'port'     => '3306',
        'charset' => 'utf8mb4',
        'driverOptions' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'")),
    'custom' => array(
        'gitVersionPath' => __DIR__."/../.git/refs/heads/master",
        'isDevMode' => true,
        'db' => array(
            //'prefix' => 'labo_',
            //'namingOverride' => array(
            //    'tables' => array(
            //        'patients' => 'ele_pat'
            //    ),
            //    'patientsColumns' => array(
            //        'id' => 'num'
            //    )
            //)
        )
    )
);