<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractEntity.php';
/**
* @Entity @Table(name="testtypes", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
**/
class TestType extends AbstractEntity implements \JsonSerializable {
	const TT_TEST = 'test';
	const TT_GROUP = 'group';
	const TT_CALCULATED = 'calculated';

	/** @Id @Column(type="string") **/
	protected $id;
	/** @Version @Column(type="integer") */
    private $version;
	/** @Column(type="string") **/
	protected $name;
	/** @Column(type="string") **/
	protected $type;
	/** @Column(type="string") **/
	protected $valueType;
	/** @Column(type="boolean") **/
	protected $instructions;
	/** @Column(type="json") **/
	protected $normals;
	/** @Column(type="string") **/
	protected $unit;
	/** @Column(type="float") **/
	protected $pointsSupplement;
	/** @Column(type="float") **/
	protected $pointsAnalyse;
	/** @Column(type="json") **/
	protected $contains;
	/** @Column(type="json") **/
	protected $containsNot;
	/** @Column(type="json") **/
	protected $depends;
	/** @Column(type="string") **/
	protected $formula;
	/** @Column(type="integer") */
	protected $round;



	public function __construct($id, $name, $valueType, $instructions=false) {
		$this->id = $id;
		setName($name);
		setValueType($valueType);
		setHasInstructions($instructions);
	}

	public static function normalizedFromArray($array, $setDefaults=false) {
		if(isset($array['id'])) { $ret['id'] = $array['id']; }
		if(isset($array['name'])) { $ret['name'] = $array['name']; }
		if(isset($array['type']) && isType($array['type'])) { $ret['type'] = $array['name']; }
		else if ($setDefaults) { $ret['type'] = TT_TEST; }
		if(isset($array['valueType'])) { $ret['valueType'] = $array['valueType']; } 
		else if ($setDefaults) { $ret['valueType'] = ValueType::TT_VOID; }
		if(isset($array['instructions'])) { $ret['instructions'] = $array['instructions']; }
		else if($setDefaults) { $ret['instructions'] = false; }
		if(isset($array['normals'])) { $ret['normals'] = $array['normals']; }
		if(isset($array['unit'])) { $ret['unit'] = $array['unit']; }
		if(isset($array['pointsSupplement'])) { $ret['pointsSupplement'] = $array['pointsSupplement']; }
		if(isset($array['pointsAnalyse'])) { $ret['pointsAnalyse'] = $array['pointsAnalyse']; }
		if(isset($array['contains'])) { $ret['contains'] = $array['contains']; }
		if(isset($array['containsNot'])) { $ret['containsNot'] = $array['containsNot']; }
		if(isset($array['depends'])) { $ret['depends'] = $array['depends']; }
		if(isset($array['formula'])) { $ret['formula'] = $array['formula']; }
		if(isset($array['round'])) { $ret['round'] = $array['round']; }
		return $ret;
	}

	public static function isType($type) {
		switch ($type) {
			case TT_TEST:
			case TT_GROUP:
			case TT_CALCULATED:
				return true;
			default:
				return false;
		}
	}

	/* getters */

	public function getId() {
		return $this->id;
	}

	public function getVersion() {
		return $this->version;
	}

	public function getName() {
		return $this->name;
	}

	public function getType() {
		return $this->type;
	}

	public function getValueType() {
		return $this->valueType;
	}

	public function canHaveInstructions() {
		return $this->instructions;
	}

	public function hasNormals() {
		return $normals != '';
	}

	public function getNormalsFor($patient) {
		if(isset($this->normals)) {
			return self::parseNormalsFor($patient, $this->normals);
		}
		return array();
	}

	public static function parseNormalsFor($patient, $normals) {
		if(isset($normals["default"])) {
			if($patient->hasDiabetes() && isset($normals["diabetes"])) {
				return self::getNormalsForGender($patient, $normals["diabetes"]);
			} else if(isset($normals["age"])) {
				foreach ($normals["age"] as $ageNormals) {
					$ageinUnit = $patient->getAge($ageNormals["unit"]);
					if($ageinUnit>$ageNormals["min"] && $ageinUnit<$ageNormals["max"]) {
						return self::getNormalsForGender($patient, $ageNormals["normals"]);
					}
				}
			}
			return self::getNormalsForGender($patient, $normals["default"]);
		} else {
			return self::getNormalsForGender($patient, $normals);
		}
	}

	private static function getNormalsForGender($patient, $normals) {
		if(isset($normals[$patient->getGender()])) {
			return $normals[$patient->getGender()];
		} else if(is_array($normals)) {
			return $normals;
		} else {
			return array();
		}
	}

	public function getNormalsTextFor($patient) {
		return self::parseNormalsTextFor($patient, $this->normals);
	}

	public static function parseNormalsTextFor($patient, $normals) {
		$normals = self::parseNormalsFor($patient, $normals);
		$normalsText = "";
		if(isset($normals["text"])) {
			$normalsText = $normals["text"];
		} else if(isset($normals['min']) && isset($normals['max'])) {
			$normalsText = sprintf(_("%s-%s"), $normals['min'], $normals['max']);
		} elseif(isset($normals['min'])) {
			$normalsText = sprintf(_(">%s"), $normals['min']);
		} elseif(isset($normals['max'])) {
			$normalsText = sprintf(_("<%s"), $normals['max']);
		}
		if(isset($normals["comment"])) {
			$normalsText = $normals["comment"]." ".$normalsText;
		}
		return $normalsText;
	}

	public function isValueNormal($value, $gender='') {
		if(!hasNormals()) { return true; }
		$normals = $this->getNormalsFor($gender);
		if(isset($normals['min']) && $value<$normals['min']) { return false; }
		if(isset($normals['max']) && $value>$normals['max']) { return false; }
		return true;	
	}

	public function validate($value) {
		return TestType\ValueType::validate($this->getValueType(), $value);
	}

	public function getUnit() {
		return $this->unit;
	}

	public function getPointsSupplement() {
		return $this->pointsSupplement;
	}

	public function getPointsAnalyse() {
		return $this->pointsAnalyse;
	}

	public function getContains() {
		return $this->contains;
	}

	public function getContainsNot() {
		if($this->containsNot!=null) {
			return $this->containsNot;
		} else {
			return array();
		}
	}

	public function getDepends() {
		return $this->depends;
	}

	public function getFormula() {
		return $this->formula;
	}

	public function getRound() {
		return $this->round;
	}

	public function calculate($sample) {
		if($this->getType()==TestType::TT_CALCULATED) {
			try{
				$value = Parser::solveIf($this->getFormula(), $sample->getTestValuesForArray($this->getDepends()));
				if($this->getRound()!=null) {
					$value = round($value, $this->getRound());
				}
				return $value;
			} catch(\Exception $e) {
				return preg_replace('/^|\ *in\W*$/', '', $e->getMessage());
			}
		} else {
			throw new \Exception(_("Trying to calculate TestType not of type \"calculated\""));
		}
	}

	public function isEnabledIn($sample) {
		$groupEnabled = true;
		foreach ($this->contains as $groupTest) {
			$test = $sample->getTestByTypeId($groupTest);
			if($test==null) {
				$groupEnabled=false;
				break;
			}
		}
		if($this->containsNot!=null) {
			foreach ($this->containsNot as $notGroupTest) {
				$test = $sample->getTestByTypeId($notGroupTest);
				if($test!=null) {
					$groupEnabled=false;
					break;
				}
			}
		}
		return $groupEnabled;
	}

	/* Setters */

	public function setName($name) {
		$this->name = $name;
	}

	public function setType($type) {
		if(!isType($type)) { throw new \Exception(_('Unknown TestType type')); }
		$this->type = $type;
	}

	public function setValueType($valueType) {
		if(!ValueType::isType($valueType)) { throw new \Exception(_('Unknown ValueType')); }
		$this->valueType = $valueType;
	}

	public function setHasInstructions($instructions) {
		$this->instructions = $instructions;
	}

	public function setNormal($gender, $minmax, $value) {
		if(	($minmax!='min' && $minmax!='max') ||
			($gender!='m' && $gender !='f' && $gender!='')) {
			throw new \Exception('unknown normal');
		}
		$this->normals[$gender][$minmax] = $value;
	}

	public function setUnit($unit) {
		$this->unit = $unit;
	}

	public function setPointsSupplement($points) {
		$this->pointsSupplement = $points;
	}

	public function setPointsAnalyse($points) {
		$this->pointsAnalyse = $points;
	}

	public function setContains($contains) {
		$this->contains = $contains;
	}

	public function setContainsNot($containsNot) {
		$this->containsNot = $containsNot;
	}

	public function setDepends($depends) {
		$this->depends = $depends;
	}

	public function setFormula($formula) {
		$this->formula = $formula;
	}

	public function setRound($round) {
		$this->round = $round;
	}

	public function JsonSerialize() {
		$retArr = array('id'=>$this->id,
			'name'=>$this->name,
			'type'=>$this->type);
		if($this->getType()==self::TT_TEST) {
			$retArr = array_merge($retArr, array(
				'valueType'=>$this->valueType,
				'instructions'=>$this->instructions));
		}
		$retArr = array_merge($retArr, array(
			'normals'=>$this->normals,
			'unit'=>$this->unit,
			'pointsSupplement'=>$this->pointsSupplement,
			'pointsAnalyse'=>$this->pointsAnalyse));
		if($this->getType()==self::TT_GROUP) {
			$retArr = array_merge($retArr, array(
				'contains'=>$this->contains,
				'containsNot'=>$this->containsNot));
		}
		if($this->getType()==self::TT_CALCULATED) {
			$retArr = array_merge($retArr, array(
				'formula'=>$this->formula,
				'round'=>$this->round));
		}
		$retArr = array_merge($retArr, array('_version'=>$this->version));
		return $retArr;
	}
}

namespace ch\romibi\labo_admin\TestType;
abstract class ValueType {
	const TT_INTEGER = 'int';
	const TT_STRING = 'str';
	const TT_BOOLEAN = 'bool';
	const TT_VOID = 'void';

	public static function isType($type) {
		switch ($type) {
			case TT_INTEGER:
			case TT_STRING:
			case TT_BOOLEAN:
			case TT_VOID:
				return true;
			default:
				return false;
		}
	}

	public static function validate($type, $value) {
		switch ($type) {
			case self::TT_INTEGER:
				return is_numeric($value);
			case self::TT_STRING:
				return !is_array($value);
			case self::TT_BOOLEAN:
				return is_bool($value);
			case self::TT_VOID:
				return $value == null || $value == "";
		}
		return false;
	}
}