# labo-admin #
labo-admin ist für die Organisation von Terminen, Testlisten & Ergebnisse von Blut- und Urin-Tests.

## Installation ##
Development:
* cd path/to/labo-admin
* php composer.phar install
(e.g. C:\xampp\php\php.exe composer.phar install)

Server:
* not yet

Browser:
* not yet

## Translation ##

### Loading Translation is not working? ###

On a Windows (XAMPP) Server only one language is supported.  
By default the servers locale is used.
To change that run (e.g.) ``set LC_ALL=de_CH`` before running xampp.

### Update Translation Template ###
Either use Poedit Pro to read form the template files or follow these steps:

* setup the development environment
* run (e.g. in cmd)  
  ``..\xampp\php\php.exe labo-admin\bin\twigcachegen.php``
* run (e.g. in bash for windows)  
  ``./_UpdateTranslationTemplate.sh``

### Update Translation ###
E.g. use Poedit
* open existing translation (or create new)
* update from pot file
* translate
* save