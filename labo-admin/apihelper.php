<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;

class ApiHelper {
	private static $instances = array();
	protected $request;
	protected $contentType = 'json';
	protected $acceptType = 'json';
	protected $statuscode = 200;
	protected $alternativeAcceptTypes;

	private function __construct() {
	}

	public static function getInstance($key='') {
		if(isset(self::$instances[$key])) {
			return self::$instances[$key];
		}
		self::$instances[$key] = new ApiHelper();
		return self::$instances[$key];
	}

	public function setAcceptType($httpaccept) {
		$types = array('json'=>-1,'xml'=>-1,'print_r'=>-1);
		$requestedContentTypes = array_map('trim',explode(',',explode(';',$httpaccept)[0]));
		foreach ($requestedContentTypes as $key => $value) {
			switch ($value) {
				case 'text/html':
				case 'text/plain':
					$types['print_r']=$key;
					break;
				case 'application/xml':
					$types['xml']=$key;
					break;
				case 'application/json':
					$types['json']=$key;
					break;
			}
		}
		while(in_array(-1, $types)) {
			if(($key = array_search(-1, $types)) !== false) {
		    	unset($types[$key]);
			}
		}
		asort($types);
		$types = array_values(array_flip($types)); //might want this later
		$this->acceptType = $types[0];
		$this->alternativeAcceptTypes = $types;
	}

	public function getContentType() {
		switch ($this->acceptType) {
			case 'print_r':
				return 'text/plain';
			case 'xml':
				return 'application/xml';
			case 'json':
			default:
				return 'application/json';
		}
	}

	public function error($e) {
		$class = get_class($e);
		$errorOut = array('message'=>'Unknown Error');
		switch($class) {
			case EntityChangedException::class:
				$this->statuscode = 409;
				$errorOut['message']=_("labo-admin error");

				$exc = array();
				$exc['type'] = get_class($e);
				$exc['code'] = $e->getCode();
				$exc['message'] = $e->getMessage();
				$exc['file'] = $e->getFile();
				$exc['line'] = $e->getLine();
				$exc['trace'] = $e->getTrace();
				$errorOut['exception'] = array($exc);

				break;
			default:
				$this->statuscode = 400;
				break;
		}
		echo $this->prepare($errorOut);
	}

	public function getStatusCode() {
		return $this->statuscode;
	}

	public function prepare($obj) {
		$obj = $this->decorate($obj);
		switch ($this->acceptType) {
			case 'xml':
				return '<error>xml not implemented yet</error>';
			case 'print_r':
				return print_r($obj,true);
			case 'json':
			default:
				return json_encode($obj);
		}
	}

	private function decorate($obj) {
		$ret = json_decode(json_encode($obj),true); //hacky?
		$ret['_links'] = 'test';
		return $ret;
	}

	public function getUpdatedResponse($response) {
		$response =  $response
			->withAddedHeader('Content-type', $this->getContentType())
			->withStatus($this->getStatusCode());
		return $response;
	}
} ?>