// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
$(document).ready(function() {
	$('.checkbox input[data-points-supplement], .checkbox input[data-points-analyse]').change(function(){
		recalc();
	});
	recalc();
});

function recalc() {
	var pointsSupplement = 4.0; // TODO: make configurable
	var pointsAnalyse = 0.0;
	$('.checkbox input[data-points-supplement], .checkbox input[data-points-analyse]').each(function() {
		var box = $(this);
		if(box.prop('checked')) {
			pointsSupplement += parseFloat(box.attr('data-points-supplement'));
			pointsAnalyse += parseFloat(box.attr('data-points-analyse'));
		}
	});

	$('#pointsSupplement').html(pointsSupplement);
	$('#pointsAnalyse').html(pointsAnalyse);
	$('#pointsTotal').html(Math.round(pointsSupplement+pointsAnalyse * 10) / 10);

	if(pointsSupplement>=24 && !$('input[name="deactivateblocker"]').prop('checked')) { // TODO: make configurable
		$('.checkbox input[data-points-supplement]').each(function() {
			var box = $(this);
			if(!box.prop('checked')) {
				box.parent('.checkbox').addClass('disabled');
				var setBy = box.attr('data-set-by');
				if(typeof setBy == 'undefined') {
					setBy = [];
				} else {
					setBy = setBy.split(',');
				}
				for (var i = setBy.length - 1; i >= 0; i--) {
					var setByElem = setBy[i];
					$('#'+setByElem).parent('.checkbox').addClass('disabled');
				}
			}
		});
	} else {
		$('.checkbox input[data-points-supplement], .checkbox input[data-sets]').each(function() {
			var box = $(this);
			if(!box.prop('checked')) {
				box.parent('.checkbox').removeClass('disabled');
			}
		});
		$('.checkbox input[data-disables]').each(function() {
			if($(this).prop('checked')) {
				var elems = $(this).attr('data-disables').split(',');
				for (var i = elems.length - 1; i >= 0; i--) {
					var elem = elems[i];
					$('#'+elem).parent('.checkbox').addClass('disabled');
				}
			}
		});
	}
}