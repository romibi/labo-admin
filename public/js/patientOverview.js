// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
$(document).ready(function() {
	var mousedownSelection;
	$("tr.sample").mousedown(function(e){ // based on https://stackoverflow.com/a/11629899
	    e.preventDefault();
	    mousedownSelection = this;
	}).mouseup(function(e){
	    e.preventDefault();
	    var selection;
	    if(mousedownSelection==this) {
	    	selection = $(this);
	    } else if($(mousedownSelection).index()<$(this).index()) {
		    selection = $(mousedownSelection).nextUntil(this).addBack().add(this); // based on https://stackoverflow.com/a/2698303
		} else {
			selection = $(this).nextUntil(mousedownSelection).addBack().add(mousedownSelection);
		}
	    selection.toggleClass("selected");
	});
});

function gotoDatasheet(type) {
	var patientId = $('input[name="patientId"]').val();
	var sampleList = [];
	$('.sample.selected').each(function() {
		sampleList.push($(this).attr('data-id'));
	});
	var address = "/patient/"+patientId+"/datasheet";
	if(sampleList.length > 0) {
		sampleList.sort(function(a,b){ return a-b; });
		address = "/patient/"+patientId+"/datasheet/"+sampleList.join('/');
	}
	if(type=='pdf') {
		window.location.href = address+".pdf";
	} else if(type=='html') {
		window.location.href = address;
	}
}

function openDeleteConfirm(sampleId) {
	$.ajax({ 
		type: "GET",
	    contentType : 'application/json',
		dataType: "json",
		url: "/api/sample/"+sampleId,
		success: function(data){
			$('#deleteItemDetails').html(render('sampleInfo', data));
			$('#deleteSampleConfirmButton').attr('data-sampleid', data.id);
			$('#deleteConfirmPopup').modal();
			console.log(data);
		},
		error: function(xhr, textStatus, thrownError) {
			$('#deleteFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
			$('#deleteFailedPopup').modal()
		}
	});
}

function deleteSample() {
	var sampleId = $('#deleteSampleConfirmButton').attr('data-sampleid');
	if(sampleId!='') {
		$.ajax({ 
			type: "DELETE",
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/sample/"+sampleId,
			success: function(data){
				location.reload();
			},
			error: function(xhr, textStatus, thrownError) {
				$('#deleteFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
				$('#deleteFailedPopup').modal()
			}
		});
	} else {
		$('#deleteFailedPopup').modal()
	}
}

function selectDoctor(save) {
	var selectelem = $('.docaddrSelect');
	if(save) {
		$.cookie("usingDoctor", selectelem.val(),{ expires : 30000, path: "/" });
	}
	var selectedOption = $('.docaddrSelect option[value="'+ selectelem.val() +'"]');
	var addr = selectedOption.attr('data-address');

	$('#docAddressPrev').html(addr.nl2br());
	if(save) {
		$('#docAddressPrev').attr('data-value-unchanged', addr);
	}
}

function revertToSavedDoc() {
	var addr = $('#docAddressPrev').attr('data-value-unchanged');
	$('#docAddressPrev').html(addr.nl2br());
	$('.docaddrSelect').val($.cookie("usingDoctor"));
}