<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
 
require_once __DIR__ . '/../vendor/autoload.php';
 
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use ch\romibi\labo_admin\LaboAdmin;

// LANGUAGE SETUP
// TODO: make nicer
$lang = "de_CH";
if(isset($_POST['lang']) && !is_array($_POST['lang']) && trim($_POST['lang']) != '') {
  setcookie("lang", $_POST['lang'], time()+60*60*24);
  $lang = trim($_POST['lang']);
} elseif(isset($_GET['lang']) && !is_array($_GET['lang']) && trim($_GET['lang']) != '') {
  setcookie("lang", $_GET['lang'], time()+60*60*24);
  $lang = trim($_GET['lang']);
}
elseif(isset($_COOKIE['lang']) && !is_array($_COOKIE['lang']) && trim($_COOKIE['lang']) != '') {
  $lang = trim($_COOKIE['lang']);
}

$language = $lang.".UTF-8"; // TODO: make configurable from clientside (on linux servers)
putenv("LANGUAGE=" . $language);
setlocale(LC_ALL, $language);

$domain = "messages"; // which language file to use
bindtextdomain($domain, __DIR__."/../locale");
bind_textdomain_codeset($domain, 'UTF-8');

textdomain($domain);

// App setup
$paths = array(__DIR__ . "/entities");

$config = require __DIR__ . '/../configs/config.php';
$isDevMode = $config['custom']['isDevMode'];

// the connection configuration
$dbParams = $config['db'];
 
$dbconfig = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
if($isDevMode) {
	$dbconfig->setAutoGenerateProxyClasses(\Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_EVAL);
}
$entityManager = EntityManager::create($dbParams, $dbconfig);

require_once 'laboadmin.php';

$laboadmin = LaboAdmin::getInstance();
$laboadmin->setBaseConfig($config);
$laboadmin->setEntityManager($entityManager);