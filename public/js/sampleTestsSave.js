// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
function sampleTestsSave(returnPath) {
	if($('#saveButton').hasClass('disabled')) return;
	$('#saveButton').addClass('disabled');

	var sample = {};

	var sampleId = $('input[name="sampleId"]').val();
	var patientId = $('input[name="patientId"]').val();

	var dateObj = $('input[name="date"]');
	var commentObj = $('textarea[name="comment"]');
	var testtypebools = $('input.testtype[type="checkbox"]');
	var testtypetexts = $('textarea.testtype');

	var status = {'runningTasks': 0, 'errors': []};
	status.newTask = function () {
		this.runningTasks++;
	};
	status.taskFinished = function () {
		this.runningTasks--;
		if(this.runningTasks==0) {
			var msg = "";
			for (var i = this.errors.length - 1; i >= 0; i--) {
				msg += this.errors[i] + '<br>';
			}
			if(msg=="") {
				window.location.href = returnPath;
			} else {
				$('#saveFailedPopup .modal-body p').html(msg);
				$('#saveFailedPopup').modal();
			}
		}
	}

	if(sampleId==''){
		sample['date'] = dateObj.val();
		sample['comment'] = commentObj.val();
		sample['tests'] = [];
	
		testtypebools.each(function() {
			if($(this).prop('checked')) {
				sample['tests'].push({"testtypeId":$(this).attr('id')});
			}
		});
	
		testtypetexts.each(function() {
			if($(this).val()!="") {
				sample['tests'].push({
						"testtypeId":$(this).attr('id'),
						"testInstructions":$(this).val()
				});
			}
		});

		$.ajax({ 
			type: "POST",
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/patient/"+patientId+"/sample",
			data: JSON.stringify(sample),
			success: function(data){
				window.location.href = returnPath;
			},
			error: function(xhr, textStatus, thrownError) {
				$('#saveFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
				$('#saveFailedPopup').modal()
			}
		});
	} else {
		status.newTask();
		/* Save Sample info itself */
		var somethingToSave = false;

		var sample = {'id': sampleId};
		
		if(dateObj.val()!=dateObj.attr('data-value-unchanged')) {
			sample['date'] = dateObj.val();
			somethingToSave = true;
		}
		if(commentObj.val()!=commentObj.attr('data-value-unchanged')) {
			sample['comment'] = commentObj.val();
			somethingToSave = true;
		}
		sample['_version']=$('input[name="version"]').val();

		if(somethingToSave) {
			status.newTask();
			$.ajax({ 
				type: "PATCH",
			    contentType : 'application/json',
				dataType: "json",
				url: "/api/sample/"+sampleId,
				data: JSON.stringify(sample),
				success: function(data){
					status.taskFinished()
				},
				error: function(xhr, textStatus, thrownError) {
					status.errors.push(getErrorFromXHR(xhr))
					status.taskFinished();
				}
			});
		}

		/* Handle adding/removing tests via checkbox */
	
		testtypebools.each(function() {
			if($(this).prop('checked')+""!=$(this).attr('data-value-unchanged')) {
				var method = '';
				if($(this).prop('checked')) {
					method = "PUT";
				} else {
					method = "DELETE";
					var skipif = $(this).attr('data-skip-delete-if');
					if(skipif) {
						var skipifs = skipif.split(',');
						for (var i = skipifs.length - 1; i >= 0; i--) {
							var value = $('#'+skipifs[i]).prop('checked');
							if(value) return;
						}
					}
				}
				status.newTask();
				updateTest(sampleId, {'testtypeId':$(this).attr('id')}, method, status);
			}
		});

		/* Handle adding/removing tests via textarea */
	
		testtypetexts.each(function() {
			if($(this).val()!=$(this).attr('data-value-unchanged')) {
				var method = '';
				var test = {'testtypeId': $(this).attr('id')};
				if($(this).val()!='' && $(this).attr('data-value-unchanged')=='') {
					method = "PUT";
					test["testInstructions"] = $(this).val();
				} else if ($(this).val()!='') {
					method = "PATCH";
					test["testInstructions"] = $(this).val();
					test["_version"] = $("#"+test.testtypeId+"_version").val()
				} else {
					method = "DELETE";
				}
				status.newTask();
				updateTest(sampleId, test, method, status);
			}
		});

		status.taskFinished();
	}
}

var updateTest = function (sampleId, test, method, status) {
	$.ajax({
		type: method,
		contentType : 'application/json',
		dataType: "json",
		url: "/api/sample/"+sampleId+"/test/"+test.testtypeId,
		data: JSON.stringify(test),
		success: function(data) {
			status.taskFinished();
		},
		error: function(xhr, textStatus, thrownError) {
			status.errors.push(getErrorFromXHR(xhr))
			status.taskFinished();
		}
	});
}