// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
$( document ).ready(function() {
	$('.calculated').each(function() {
		var field = $(this);
		var elems = field.attr('data-depends').split(',');

		var scope = {};
		for (var i = elems.length - 1; i >= 0; i--) {
			scope[elems[i]] = 0;
		}

		var formula = field.attr('data-formula');
		var mathnode = math.parse(formula, scope);
		field.data('scope', scope);
		field.data('compiledFormula', mathnode.compile());

		for (var i = elems.length - 1; i >= 0; i--) {
			var elem = elems[i];
			$('#'+elem).keyup(function() {
				recalcCalculated(field);
			});
		}
		recalcCalculated(field);
	});
});

function recalcCalculated(field) {
	var elems = field.attr('data-depends').split(',');
	var scope = field.data('scope');
	for (var i = elems.length - 1; i >= 0; i--) {
		var value = $('#'+elems[i]).val();
		if(typeof value == 'undefined') value = 0;
		scope[elems[i]] = value;
	}
	var formula = field.data('compiledFormula');
	var round = field.attr('data-round');
	field.val(math.round(formula.eval(scope), round));
}