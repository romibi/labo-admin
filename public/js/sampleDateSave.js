// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
function sampleDateSave(returnPath) {
	if($('#saveButton').hasClass('disabled')) return;
	$('#saveButton').addClass('disabled');

	var sample = {};

	var sampleId = $('input[name="sampleId"]').val();
	var patientId = $('input[name="patientId"]').val();

	var dateObj = $('input[name="date"]');

	var somethingToSave = false;

	var sample = {'id': sampleId};
	
	if(dateObj.val()!=dateObj.attr('data-value-unchanged')) {
		sample['date'] = dateObj.val();
		somethingToSave = true;
	}
	sample['_version']=$('input[name="version"]').val();

	if(somethingToSave) {
		$.ajax({ 
			type: "PATCH",
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/sample/"+sampleId,
			data: JSON.stringify(sample),
			success: function(data){
				window.location.href = returnPath;
			},
			error: function(xhr, textStatus, thrownError) {
				$('#saveFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
				$('#saveFailedPopup').modal()
			}
		});
	}
}