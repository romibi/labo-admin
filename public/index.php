<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
require '../vendor/autoload.php';
require_once '../labo-admin/bootstrap.php';
require_once '../labo-admin/apihelper.php';
require_once '../labo-admin/web/TwigExtension/MathSolve.php';

$config = require __DIR__ . '/../configs/config.php';

require_once($config['slim']['templates.path'].'/dataSheetPdf.php');

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use ch\romibi\labo_admin;
use ch\romibi\labo_admin\ApiHelper;

$app = new \Slim\App($config['slim']);

//$app->view(new \Slim\Views\Twig($config['twig']));
$container = $app->getContainer();
$container['view'] = function ($container) use ($config) {
    $view = new \Slim\Views\Twig($config['slim']['templates.path'], $config['twig']);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    $view->addExtension(new Twig_Extensions_Extension_I18n());
    if($config['custom']['isDevMode']) {
	    $view->addExtension(new Twig_Extension_Debug());
    }
    $view->addExtension(new \ch\romibi\labo_admin\Twig\MathSolve());

    return $view;
};

$laboadmin->setSlimAppObj($app);

/* Routes */
require '../labo-admin/routes/api.php';
require '../labo-admin/routes/web.php';

$app->run();