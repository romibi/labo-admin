<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use ch\romibi\labo_admin;
use ch\romibi\labo_admin\ApiHelper;

$app->group('', function () use ($app, $laboadmin, $container) {
	$app->get('/', function ($request, $response) use ($app, $laboadmin, $container) {
		$container['view']->render($response, 'index.twig');
	});

	$app->group('/patient', function () use ($app, $laboadmin, $container) {
		$app->get('/new', function ($request, $response) use ($app, $laboadmin, $container) {
			$container['view']->render($response, 'patient.twig', array('patient'=>array()));
		});
		$app->get('/{patientId}', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$patient = $laboadmin->patient()->get($args['patientId']);

			$response = $laboadmin->setCookie('lastPatientId', $args['patientId'], time()+60*60*24);

			$addresses = $laboadmin->config()->getJSON('general.addresses');
			$usingDoctor = $laboadmin->getCookie('usingDoctor');

			$container['view']->render($response, 'patientOverview.twig', array('patient'=>$patient, 'addresses'=>$addresses, 'usingDoctor'=>$usingDoctor));
			return $response;
		});
		$app->get('/{patientId}/edit', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$patient = $laboadmin->patient()->get($args['patientId']);
			$container['view']->render($response, 'patient.twig', array('patient'=>$patient));
		});
		$app->get('/{patientId}/sample/new', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$layout = $laboadmin->config()->getJSON('layout.testtype.web');
			$testtypes = $laboadmin->testtype()->listById();
			$patient = $laboadmin->patient()->get($args['patientId']);
			$container['view']->render($response, 'sampleTests.twig', array('patient'=>$patient, 'layout'=>$layout, 'testtypes'=>$testtypes));
		});
		$app->get('/{patientId}/datasheet.pdf', function($request, $response, $args) use ($app, $laboadmin, $container) {
			$layout = $laboadmin->config()->getJSON('layout.testtype.print');
			$testtypes = $laboadmin->testtype()->listById();
			$patient = $laboadmin->patient()->get($args['patientId']);
			$samples = $patient->getSamples();
			$addresses = $laboadmin->config()->getJSON('general.addresses');
			$usingDoctor = $laboadmin->getCookie('usingDoctor');
			$doctor = $addresses[$usingDoctor]->text;
			generatePDF($samples, $patient, $testtypes, $layout, $doctor, $laboadmin);
			return $response->withAddedHeader('Content-type', 'application/pdf');
		});
		$app->get('/{patientId}/datasheet', function($request, $response, $args) use ($app, $laboadmin, $container) {
			$layout = $laboadmin->config()->getJSON('layout.testtype.print');
			$testtypes = $laboadmin->testtype()->listById();
			$patient = $laboadmin->patient()->get($args['patientId']);
			$samples = $patient->getSamples();
			$addresses = $laboadmin->config()->getJSON('general.addresses');
			$usingDoctor = $laboadmin->getCookie('usingDoctor');
			$container['view']->render($response, 'dataSheet.twig', array('samples'=>$samples, 'layout'=>$layout, 'testtypes'=>$testtypes, 'patient'=>$patient, 'addresses'=>$addresses, 'usingDoctor'=>$usingDoctor));
		});
		$app->get('/{patientId}/datasheet/{sampleIds:.*}.pdf', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$sampleIds = explode('/', $args['sampleIds']);
			$samples = array();
			foreach ($sampleIds as $sampleId) {
				$samples[] = $laboadmin->sample()->get($sampleId);
			}

			$layout = $laboadmin->config()->getJSON('layout.testtype.print');
			$testtypes = $laboadmin->testtype()->listById();
			$patient = $laboadmin->patient()->get($args['patientId']);
			$addresses = $laboadmin->config()->getJSON('general.addresses');
			$usingDoctor = $laboadmin->getCookie('usingDoctor');
			$doctor = $addresses[$usingDoctor]->text;
			generatePDF($samples, $patient, $testtypes, $layout, $doctor, $laboadmin);
			return $response->withAddedHeader('Content-type', 'application/pdf');
		});
		$app->get('/{patientId}/datasheet/{sampleIds:.*}', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$sampleIds = explode('/', $args['sampleIds']);
			$samples = array();
			foreach ($sampleIds as $sampleId) {
				$samples[] = $laboadmin->sample()->get($sampleId);
			}

			$layout = $laboadmin->config()->getJSON('layout.testtype.print');
			$testtypes = $laboadmin->testtype()->listById();
			$patient = $laboadmin->patient()->get($args['patientId']);
			$addresses = $laboadmin->config()->getJSON('general.addresses');
			$usingDoctor = $laboadmin->getCookie('usingDoctor');
			$container['view']->render($response, 'dataSheet.twig', array('samples'=>$samples, 'layout'=>$layout, 'testtypes'=>$testtypes, 'patient'=>$patient, 'addresses'=>$addresses, 'usingDoctor'=>$usingDoctor));
		});
	});

	$app->group('/sample', function () use ($app, $laboadmin, $container) {
		$app->get('/{sampleId:\d+}', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$sample = $laboadmin->sample()->get($args['sampleId']);
			$testtypes = $laboadmin->testtype()->listById();
			$returnPath = $request->getParam('returnPath');
			$layout = $laboadmin->config()->getJSON('layout.testtype.web');
			$container['view']->render($response, 'sampleView.twig', array('sample'=>$sample, 'layout'=>$layout, 'testtypes' => $testtypes, 'returnPath'=>$returnPath));
		});
		$app->get('/{sampleId:\d+}/edit/{mode:(?:tests|date|results)}', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$sample = $laboadmin->sample()->get($args['sampleId']);
			$testtypes = $laboadmin->testtype()->listById();
			$template = '';
			$returnPath = $request->getParam('returnPath');
			switch ($args['mode']) {
				case 'tests':
					$template = 'sampleTests.twig';
					break;
				case 'date':
					$template = 'sampleDate.twig';
					break;
				case 'results':
					$template = 'sampleResults.twig';
					break;
			}
			$layout = $laboadmin->config()->getJSON('layout.testtype.web');
			$container['view']->render($response, $template, array('sample'=>$sample, 'layout'=>$layout, 'testtypes' => $testtypes, 'returnPath'=>$returnPath));
		});
		$app->get('/list/{listname:(?:no-date|not-done|not-seen|marked)}', function ($request, $response, $args) use ($app, $laboadmin, $container) {
			$samples=null;
			$listname = $args['listname'];
			$samples = $laboadmin->sample()->list($listname);
			switch ($listname) {
				case 'no-date':
					$defaultAction = '/edit/date';
					break;
				case 'not-done':
					$defaultAction = '/edit/results';
					break;
				case 'not-seen':
				case 'marked':
				default:
					$defaultAction = '';
					break;
			}
			$container['view']->render($response, 'samples.twig', array('samples'=>$samples, 'listname'=>$listname, 'defaultAction'=>$defaultAction));
		});
	});

	$app->get('/credits', function ($request, $response) use ($app, $laboadmin, $container) {
		$container['view']->render($response, 'credits.twig');
	});
})->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) use ($app, $laboadmin) {
    // Use the PSR 7 $request object
    $laboadmin->setRequestObj($request);
    $laboadmin->setResponseObj($response);

    // generate menu
	$menu = $laboadmin->web()->menu();
	$menu->addLink('/', _('Tasks'), array('icon'=>'images/basic-ui/home153.svg'));
	$nodateCount = $laboadmin->sample()->count('no-date');
	$menu->addLink('/sample/list/no-date', _('Tests with no dates'), array('count'=>$nodateCount, 'icon'=>'images/lodgicons/calendar70.svg'));
	$notdoneCount = $laboadmin->sample()->count('not-done');
	$menu->addLink('/sample/list/not-done', _('Not done tests'), array('count'=>$notdoneCount, 'icon'=>'images/medical-icons/blood11.svg'));
	$notseenCount = $laboadmin->sample()->count('not-seen');
	$menu->addLink('/sample/list/not-seen', _('Unseen results'), array('count'=>$notseenCount, 'icon'=>'images/medical-icons/human90.svg'));
	$menu->addLink('/sample/list/marked', _('Marked'), array('icon'=>'images/basic-ui/bookmark49.svg'));
	
	$lastPatientId = $laboadmin->getCookie('lastPatientId');
	if($lastPatientId!=null) {
		$patient = $laboadmin->patient()->get($lastPatientId);
		if($patient!=null) {
			$menu->addLink('/patient/'.$lastPatientId, _('Last Patient')." (".$patient->getName().")", array('position'=>'right', 'icon'=>'images/medical-icons/person278.svg'));
		}
	}

	if($laboadmin->config()->get('layout.footer.showLanguageSwitch', 'false')=='true') {
		$laboadmin->web()->footer()->addLink('?lang=de_CH', _('German'), array());
		$laboadmin->web()->footer()->addLink('?lang=fr_CH', _('French'), array());
		$laboadmin->web()->footer()->addLink('?lang=en_GB', _('English'), array());
	}

	$laboadmin->web()->footer()->addLink('/credits', _('Credits'), array('position'=>'right', 'class'=>'credits'));
	$laboadmin->web()->footer()->addLink('', $laboadmin->web()->getVersionString(), array('position'=>'right', 'class'=>'versioninfo'));

	// add menu to view
	$this->get('view')->getEnvironment()->addGlobal('menu', $laboadmin->web()->menu()->getLinks());
	if($laboadmin->config()->get('layout.footer.show', 'true') == 'true') {
		$this->get('view')->getEnvironment()->addGlobal('footer', $laboadmin->web()->footer()->getLinks());
	}
	// add other global variables to view
	$this->get('view')->getEnvironment()->addGlobal('config', $laboadmin->config());
	$this->get('view')->getEnvironment()->addGlobal('hidedemoalert', $laboadmin->getCookie('hidedemoalert'));
	$this->get('view')->getEnvironment()->addGlobal('versionString', $laboadmin->web()->getVersionString(true));
	return $next($request, $response);
});