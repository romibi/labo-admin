// Copyright (c) 2009 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt)
function filter(site) {
  var rows = $("table").find("tr:not(.thead)").show();

  var dateFilter=null;
  if($('#dateFilter').length) {
    dateFilter = $('#dateFilter').val().split(" ");
    dateFilter = $.grep(dateFilter,function(n){ return(n) });
  }

  var patientIdFilter = $('#patientIdFilter').val().split(" ");
  patientIdFilter = $.grep(patientIdFilter,function(n){ return(n) });

  var patientNameFilter = $('#patientNameFilter').val().split(" ");
  patientNameFilter = $.grep(patientNameFilter,function(n){ return(n) });

  var commentFilter = $('#commentFilter').val().split(" ");
  commentFilter = $.grep(commentFilter,function(n){ return(n) });

  var filterStrings = {
    patientId: $('#patientIdFilter').val(),
    patientName: $('#patientNameFilter').val(),
    comment: $('#commentFilter').val()
  };
  if(dateFilter!=null) {
    filterStrings.date = $('#dateFilter').val();
  }

  $.cookie(site+"Filters", JSON.stringify(filterStrings), { expires : 30000 });

  if(dateFilter !=null && dateFilter.length>0) {
    $.each(dateFilter, function(i, v){
      var filterkey = fixupFilterkey(v);
      rows.find('.date').filter(function(){
        return !new RegExp(filterkey[0], filterkey[1]).test($(this).text());
      }).parent().hide();
    });
  }

  if(patientIdFilter.length>0) {
    $.each(patientIdFilter, function(i, v){
      var filterkey = fixupFilterkey(v);
      rows.find('.patient').filter(function(){
        var classes = this.className.split(' ');

        var found = false;

        for (index = 0; index < classes.length && !found; ++index) {
          var c = classes[index].replaceAll('patientID_', '');
          if(new RegExp(filterkey[0], filterkey[1]).test(c)) {
            found = true;
          }
        }
        return !found;
      }).parent().hide();
    });
  }

  if(patientNameFilter.length>0) {
    $.each(patientNameFilter, function(i, v){
      var filterkey = fixupFilterkey(v);
      rows.find('.patient').filter(function(){
        return !new RegExp(filterkey[0], filterkey[1]).test($(this).text());
      }).parent().hide();
    });
  }

  if(commentFilter.length>0) {
    $.each(commentFilter, function(i, v){
      var filterkey = fixupFilterkey(v);
      rows.find('.comment').filter(function(){
        return !new RegExp(filterkey[0], filterkey[1]).test($(this).text());
      }).parent().hide();
    });
  }
}

String.prototype.replaceAll = function (find, replace) {
  return this.split(find).join(replace);
}

/* by sean bright on http://stackoverflow.com/a/1144788 */
function escapeRegExpExceptWildcards(string) {
    return string.replace(/([.+=!:{}()|\[\]\/\\])/g, "\\$1");
}

function fixupFilterkey(string) {
  var newstring;
  var modifiers = '';
  if(string.indexOf('*') > -1 || string.indexOf('?') > -1 || string.indexOf('^') > -1 || string.indexOf('$') > -1) {
    if(string.indexOf('^') > -1 || string.indexOf('$') > -1) {
      newstring = string;
    } else {
      newstring = escapeRegExpExceptWildcards(string).replaceAll('?', '.').replaceAll('*', '.*?');
      modifiers = 'i';
    }
  } else {
    newstring = '.*' + string + '.*';
    modifiers = 'i';
  }
  return [newstring, modifiers];
}