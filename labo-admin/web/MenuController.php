<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
class MenuController {
	protected $menuLinks = null;
	public function __construct() {
		$this->menuLinks = array('left' => array(),
								'right' => array());
		/*$menuLinks = array(
			array('href'=>'/', 'text'=>'Aufgaben'),
			array('href'=>'/sample/list/no-date', 'text'=>'Test-Datum nachtragen'),
			array('href'=>'/sample/list/not-done', 'text'=>'Nicht erledigte Tests'),
			array('href'=>'/sample/list/not-seen', 'text'=>'Ungesehene Resultate'),
			array('href'=>'/sample/list/marked', 'text'=>'Markiertes'),
			array('href'=>'/credits', 'text'=>'Credits')
		));*/
	}

	public function addLink($link, $text, $params=array()) {
		$link = array('href'=>$link, 'text'=>$text);
		$position = 'left';
		foreach ($params as $key => $value) {
			switch ($key) {
				// wanted fallthroughs
				case 'class':
				case 'count':
				case 'icon':
					$link[$key]=$value;
					break;
				case 'position':
					$position = $value;
					break;
				default:
					break;
			}
		}
		$this->menuLinks[$position][] = $link;
	}

	public function getLinks() {
		return $this->menuLinks;
	}
}