<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractEntity.php';
require_once __DIR__.'/../laboadmin.php';
use ch\romibi\labo_admin\LaboAdmin;
/**
* @Entity @Table(name="config", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
**/
class Config extends AbstractEntity implements \JsonSerializable {
	/** @Id @Column(type="string") **/
	protected $key;
	/** @Column(type="string") **/
	protected $value;
	/** @Version @Column(type="integer") */
    private $version = 1;

	public function __construct($key) {
		$this->key = $key;
	}

	public function getKey() {
		return $this->key;
	}

	public function setKey($key) {
		$this->key = $key;
	}

	public function getValue() {
		return $this->value;
	}

	public function setValue($value) {
		$this->value = $value;
	}

	public function getValueJSON() {
		return json_decode($this->value);
	}

	public function setValueJSON($value) {
		$this->value = json_encode($value);
	}

	public function getVersion() {
		return $this->version;
	}

	public static function normalizedFromArray($array, $setDefaults=false) {
		if(isset($array['key'])) { $ret['key'] = $array['key']; }
		if(isset($array['value'])) { $ret['value'] = $array['value']; }
		return $ret;
	}

	public function JsonSerialize()
	{
		return array('key'=>$this->key,
			'value'=>$this->value,
			'_version'=>$this->version
		);
	}
}