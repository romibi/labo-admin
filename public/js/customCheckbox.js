// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
$(document).ready(function() {
	$('.checkbox').on('click', function( event ) {
		if(!($(event.target).is('input'))) {
			$('input', this).trigger('click');
			return;
		}

		if($('input', this).prop('checked') && !($(this).hasClass('checked'))) {
			$(this).addClass('checked');
		} else if(!($('input', this).prop('checked')) && $(this).hasClass('checked')) {
			$(this).removeClass('checked');
		}
	});
	$('.checkbox input').on('click', function( event ) {
		var checkbox = $(this);
		if(checkbox.parent('.checkbox').hasClass('disabled') || checkbox.prop('readonly')==true) {
			return false;
		}

		if(checkbox.attr('data-disables')!=null && checkbox.attr('data-disables')!='') {
			var elems = checkbox.attr('data-disables').split(',');
			for (var i = elems.length - 1; i >= 0; i--) {
				var elem = elems[i];
				$('#'+elem).parent('.checkbox').toggleClass('disabled');
			}
		}

		if(checkbox.attr('data-sets')!=null && checkbox.attr('data-sets')!='') {
			var elems = checkbox.attr('data-sets').split(',');
			for (var i = elems.length - 1; i >= 0; i--) {
				var elem = elems[i];
				var check = checkbox.prop('checked')
				$('#'+elem).prop('checked', check);
				if(check) {
					$('#'+elem).parent('.checkbox').addClass('checked');
				} else {
					$('#'+elem).parent('.checkbox').removeClass('checked');
				}

			}
		}
	});
	$('.checkbox input[data-depends]').each(function(){
		var box = $(this);
		var elems = box.attr('data-depends').split(',');
		for (var i = elems.length - 1; i >= 0; i--) {
			var elem = elems[i];
			$('#'+elem).change(function() {
				dependentCheckboxCheck(box);
			});
		}
		dependentCheckboxCheck(box);
	});

	$('.checkbox input[data-sets]').each(function(){
		var box = $(this);
		var elems = box.attr('data-sets').split(',');
		box.parent('.checkbox').on('mouseenter mouseleave', function(event) {
			for (var i = elems.length - 1; i >= 0; i--) {
				var elem = elems[i];
				if(event.type=='mouseenter') {
					$('#'+elem).parent('.checkbox').addClass('highlighted');
				} else if(event.type=='mouseleave') {
					$('#'+elem).parent('.checkbox').removeClass('highlighted');
				}
			}
		});
		for (var i = elems.length - 1; i >= 0; i--) {
			var elem = elems[i];
			var setBy = $('#'+elem).attr('data-set-by');
			if(typeof setBy == 'undefined') {
				setBy = [];
			} else {
				setBy = setBy.split(',');
			}
			setBy.push(box.attr('id'));

			$('#'+elem).attr('data-set-by', setBy.join(','));
		}
	});

	$('.checkbox input[data-sets]').each(function(){
		var box = $(this);
		var elems = box.attr('data-sets').split(',');
		box.parent('.checkbox').on('mouseenter mouseleave', function(event) {
			for (var i = elems.length - 1; i >= 0; i--) {
				var elem = elems[i];
				if(event.type=='mouseenter') {
					$('#'+elem).parent('.checkbox').addClass('highlighted');
				} else if(event.type=='mouseleave') {
					$('#'+elem).parent('.checkbox').removeClass('highlighted');
				}
			}
		});
	});

	$('.checkbox input[data-disables]').each(function(){
		var box = $(this);
		var elems = box.attr('data-disables').split(',');
		box.parent('.checkbox').on('mouseenter mouseleave', function(event) {
			for (var i = elems.length - 1; i >= 0; i--) {
				var elem = elems[i];
				if(event.type=='mouseenter') {
					$('#'+elem).parent('.checkbox').addClass('blurred');
				} else if(event.type=='mouseleave') {
					$('#'+elem).parent('.checkbox').removeClass('blurred');
				}
			}
		});
	});
});

function dependentCheckboxCheck(box) {
	var enabled = true;
	var elems = box.attr('data-depends').split(',');
	for (var i = elems.length - 1; i >= 0; i--) {
		var elem = elems[i];
		var elemChecked = $('#'+elem).prop('checked');
		if(!elemChecked) {
			enabled=false;
			break;
		}
	}
	box.prop('checked', enabled);
	if(enabled) {
		box.parent('.checkbox').addClass('checked');
	} else {
		box.parent('.checkbox').removeClass('checked');
	}
}