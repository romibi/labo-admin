<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractController.php';
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class SampleController extends AbstractController {
	protected $entityManager;
	protected $repository;

	public function __construct($entityManager) {
		$this->entityManager = $entityManager;
		$this->repository = $entityManager->getRepository('ch\romibi\labo_admin\Sample');
	}

	public static function getDefaultFilter($name) {
		$defaultFilters = array(
			'no-date' => array('date'=>''),
			'not-done' => array('done'=>'0', 'date'=> '<='.date('Y-m-d')),
			'not-seen' => array('seen'=>'0', 'done'=>'1'),
			'marked' => array('marked'=>'1')
		);
		return $defaultFilters[$name];
	}

	public function list($params) {
		$parameters = $params;
		if(!is_array($params) && is_string($params)) {
			$parameters = self::getDefaultFilter($params);
		}
		$samples = $this->findByCustom($parameters)->getValues();
		foreach($samples as $key => $sample) {
			$samples[$key] = $sample->getDetachedFlat($this->entityManager, false);
		}
		return $samples;
	}

	public function count($params) {
		$parameters = $params;
		if(!is_array($params) && is_string($params)) {
			$parameters = self::getDefaultFilter($params);
		}
		$results = $this->findByCustom($parameters);
		return $results->count();
	}

	private function findByCustom($params) {
		if(count($params)<1) {
			return false;
		}
		$criteria = Criteria::create();
		$first  = true;
		foreach ($params as $key => $value) {
			$expressionType = self::getExpressionTypeFromFilterValue($value);
			$value = self::getValueFromFilterValue($value);
			switch ($key) {
				case 'date':
					if($value=='') {
						$value=null;
					} else {
						$value=new \DateTime($value);
					}
					break;
				case 'comment':
					if($value=='') {
						$value=null;
					}
					break;
				case 'done':
				case 'seen':
				case 'marked':
					if($value==='false') $value='0';
					if($value==='true') $value='1';
					break;
				default:
					continue;
			}

			if($first) {
				$criteria->where(self::getExpression($key, $expressionType, $value));
			} else {
				$criteria->andWhere(self::getExpression($key, $expressionType, $value));
			}
			$first = false;
		}
		return $this->repository->matching($criteria);
	}

	public function get($id) {
		return $this->repository->find($id);
	}

	public function patch($id, $object) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if($id != $object['id']) { throw new \Exception('multiple different ids provided'); }
		if(!isset($object['_version'])) { $object['_version']=-1; }
		try {
			$sample = $this->repository->find($id, LockMode::OPTIMISTIC, $object['_version']);
			$sample->updateFromArray($object);
			$this->entityManager->flush();
			return $sample;
		} catch(OptimisticLockException $e) {
			throw new EntityChangedException(_("Someone already changed something in this sample"));
		}
	}

	public function delete($id) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if($id != $object['id']) { throw new \Exception('multiple different ids provided'); }
		$sample = $this->repository->find($id);
		$this->entityManager->remove($sample);
		$this->entityManager->flush();
		return true;
	}

	public function putTests($sampleid, $objects) {
		if(!isset($objects['tests']))
			return false;
		$tests = $objects['tests'];
		$testController = LaboAdmin::getInstance()->test();
		foreach ($tests as $test) {
			$testController->put($sampleid, $test['testtypeId'], $test);
		}
		return true; // TODO: improve?
	}
}
?>