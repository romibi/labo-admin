<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractEntity.php';
require_once 'Patient.php';
require_once 'Test.php';
require_once __DIR__.'/../laboadmin.php';
use ch\romibi\labo_admin\LaboAdmin;
/**
* @Entity @Table(name="samples", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
**/
class Sample extends AbstractEntity implements \JsonSerializable {
	/** @Id @Column(type="integer") @GeneratedValue **/
	protected $id;
	/** @Version @Column(type="integer") */
    private $version;
	/** @Column(type="date") **/
	protected $date;
	/** @Column(type="string") **/
	protected $comment;
	/** @Column(type="string") **/
	protected $resultcomment;
	/** @Column(type="boolean") **/
	protected $done;
	/** @Column(type="boolean") **/
	protected $seen;
	/** @Column(type="boolean") **/
	protected $marked;

	/**
	  * @ManyToOne(targetEntity="Patient")
	  * @JoinColumn(name="patientId", referencedColumnName="id")
	  */
    protected $patient;
    /** @OneToMany(targetEntity="Test", mappedBy="sample") **/
    protected $tests;

	public function __construct($patient, $date=null) {
		$this->patient = $patient;
		$this->tests = new ArrayCollection();
		if($date) {
			$this->date = $date;
		}
	}

	public static function normalizedFromArray($array, $setDefaults=false) {
		if(isset($array['id'])) { $ret['id'] = $array['id']; }
		if(isset($array['date']) && $array['date']!="") { $ret['date'] = new \DateTime($array['date']); }
		if(isset($array['comment'])) { $ret['comment'] = $array['comment']; }
		if(isset($array['resultcomment'])) { $ret['resultcomment'] = $array['resultcomment']; }
		if(isset($array['done'])) { $ret['done'] = $array['done']; }
		else if($setDefaults) { $ret['done'] = false; }
		if(isset($array['seen'])) { $ret['seen'] = $array['seen']; }
		else if($setDefaults) { $ret['seen'] = false; }
		if(isset($array['marked'])) { $ret['marked'] = $array['marked']; }
		else if($setDefaults) { $ret['marked'] = false; }
		if(isset($array['patientId'])) { $ret['patient'] = LaboAdmin::getInstance()->patient()->get($array['patientId']); }
		//TODO: validate more?
		return $ret;
	}

	public function getId() {
		return $this->id;
	}

	public function getVersion() {
		return $this->version;
	}

	public function getDate() {
		return $this->date;
	}

	public function getComment() {
		return $this->comment;
	}

	public function getResultComment() {
		return $this->resultcomment;
	}

	public function isDone() {
		return $this->done;
	}

	public function isSeen() {
		return $this->seen;
	}

	public function isMarked() {
		return $this->marked;
	}

	public function setDate($date) {
		$this->date = $date;
	}

	public function setComment($comment) {
		$this->comment = $comment;
	}

	public function setResultComment($resultcomment) {
		$this->resultcomment = $resultcomment;
	}

	public function setDone($done=true) {
		$this->done = $done;
	}

	public function setSeen($seen) {
		$this->seen = $seen;
	}

	public function setMarked($marked) {
		$this->marked = $marked;
	}

	public function getPatient() {
		return $this->patient;
	}

	public function getTests() {
		return $this->tests;
	}

	public function hasValues() {
		foreach ($this->tests as $test) {
			if($test->getValue()!=null) return true;
		}
		return false;
	}

	public function getTestByTypeId($testtypeId) {
		if($this->tests==null) return null;
		foreach ($this->tests as $test) {
			if($test->getTestType()->getId()==$testtypeId) {
				return $test;
			}
		}
	}

	public function getTestValuesForArray($testtypeIds) {
		$values = array();
		foreach ($testtypeIds as $testtypeId) {
			$test = $this->getTestByTypeId($testtypeId);
			if($test != null) {
				$value = $test->getValue();
				if($value != null) {
					$values[$testtypeId] = $value;
				}
			}
		}
		return $values;
	}

	public function getDetachedFlat($entityManager, $removePatient = true, $removeTests = true) {
		$entityManager->detach($this);
		if($removePatient)
			$this->patient = null;
		if($removeTests)
			$this->tests = null;
		return $this;
	}

	public function JsonSerialize()
	{
		$date = null;
		if($this->date)
			$date = date_format($this->date,'Y-m-d');
		$ret = array('id'=>$this->id,
			'date'=>$date,
			'comment'=>$this->comment,
			'resultcomment'=>$this->resultcomment,
			'done'=>$this->done,
			'seen'=>$this->seen,
			'marked'=>$this->marked,
			'_version'=>$this->version
		);
		if($this->patient)
			$ret['_embedded']['patient'] = $this->patient;
		if($this->tests)
			$ret['_embedded']['tests'] = $this->tests->getValues();
		return $ret;
	}
}