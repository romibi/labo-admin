<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractController.php';
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class PatientController extends AbstractController {
	protected $entityManager;
	protected $repository;
	public function __construct($entityManager) {
		$this->entityManager = $entityManager;
		$this->repository = $entityManager->getRepository('ch\romibi\labo_admin\Patient');
	}

	public function search($key) {
		$criteria = Criteria::create();
		$criteria
		  ->where($criteria->expr()->contains('id', $key))
		  ->orWhere($criteria->expr()->contains('name', $key));
		$patients = $this->repository
		  ->matching($criteria);

		$iterator = $patients->getIterator();
		$iterator->uasort(function ($a, $b) use ($key) {
			if(strpos($a->getId(), $key)!==false) {
				return -strnatcmp($a->getId(), $b->getId());
			} else if(strpos($a->getName(), $key)!==false) {
				return -strnatcmp($a->getName(), $b->getName());
			} else {
				return -strnatcmp($a->getId(), $b->getId());
			}
		});
		return (new \Doctrine\Common\Collections\ArrayCollection(iterator_to_array($iterator)))->getValues();
	}

	public function get($id) {
		return $this->repository->find($id);
	}

	public function put($id, $object) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if(strtolower($id)!= strtolower($object['id'])) { throw new \Exception(_('Multiple different ids provided')); }
		$newpat = Patient::fromArray($object);
		$this->entityManager->persist($newpat);
		$this->entityManager->flush();
		return $newpat;
	}

	public function patch($id, $object) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if(strtolower($id)!= strtolower($object['id'])) { throw new \Exception(_('Multiple different ids provided')); }
		if(!isset($object['_version'])) { $object['_version']=-1; }
		try {
			$patient = $this->repository->find($id, LockMode::OPTIMISTIC, $object['_version']);
			$patient->updateFromArray($object);
			$this->entityManager->flush();
			return $patient;
		} catch(OptimisticLockException $e) {
			throw new EntityChangedException(_("Someone already changed something in this patient"));
		}
	}

	public function delete($id) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if(strtolower($id)!= strtolower($object['id'])) { throw new \Exception(_('Multiple different ids provided')); }
		$patient = $this->repository->find($id);
		$this->entityManager->remove($patient);
		$this->entityManager->flush();
		return true;
	}

	public function getSamples($id) {
		$patient = $this->get($id);
		$samples = $this->get($id)->getSamples()->getValues();
		foreach ($samples as $key => $sample) {
			$samples[$key] = $sample->getDetachedFlat($this->entityManager);
		}
		return array('samples'=>$samples, 
			'_embedded'=>array('patient'=>$patient));
	}

	public function postSample($id, $obj) {
		if(!isset($obj['patientId'])) { $obj['patientId']=$id; }
		if(strtolower($id)!=strtolower($obj['patientId'])) { throw new \Exception('Multiple different ids provided'); }
		$patient = $this->get($id);
		$newsample = Sample::fromArray($obj);
		$this->entityManager->persist($newsample);
		$this->entityManager->flush();
		if(LaboAdmin::getInstance()->sample()->putTests($newsample->getId(),$obj)) {
			$this->entityManager->refresh($newsample);
		}
		return $newsample;
	}
}
?>