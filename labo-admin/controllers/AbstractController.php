<?php
// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
use Doctrine\Common\Collections\Criteria;

class AbstractController {
	protected static function getExpressionTypeFromFilterValue($filterValue) {
		$expressionType='=';
		if(substr($filterValue, 0, 5) === '&lt;=') {
			$expressionType = '<=';
		} else if(substr($filterValue, 0, 5) === '&gt;=') {
			$expressionType = '>=';
		} else if(substr($filterValue, 0, 4) === '&lt;') {
			$expressionType = '<';
		} else if(substr($filterValue, 0, 4) === '&gt;') {
			$expressionType = '>';
		} else if(substr($filterValue, 0, 2) === '<=') {
			$expressionType = '<=';
		} else if(substr($filterValue, 0, 2) === '>=') {
			$expressionType = '>=';
		} else if(substr($filterValue, 0, 1) === '<') {
			$expressionType = '<';
		} else if(substr($filterValue, 0, 1) === '>') {
			$expressionType = '>';
		} else if(substr($filterValue, 0, 1) === '!') {
			$expressionType = '!';
		} else if(substr($filterValue, 0, 1) === '~') {
			$expressionType = '~';
		}
		return $expressionType;
	}

	protected static function getValueFromFilterValue($filterValue) {
		if(substr($filterValue, 0, 5) === '&lt;=' || substr($filterValue, 0, 5) === '&gt;=') {
			return substr($filterValue, 5);
		} else if(substr($filterValue, 0, 4) === '&lt;' || substr($filterValue, 0, 4) === '&gt;') {
			return substr($filterValue, 4);
		} else if(substr($filterValue, 0, 2) == '<=' || substr($filterValue, 0, 2) == '>=') {
			return substr($filterValue, 2);
		} else if(substr($filterValue, 0, 1) === '!' || substr($filterValue, 0, 1) === '~' || substr($filterValue, 0, 1) === '<' || substr($filterValue, 0, 1) === '>') {
			return substr($filterValue, 1);
		}
		return $filterValue;
	}

	protected static function getExpression($key, $expressionType, $value) {
		$expr = Criteria::expr();
		if($value=='') return $expr->isNull($key);
		switch ($expressionType) {
			case '<=':
				return $expr->lte($key, $value);
			case '>=':
				return $expr->gte($key, $value);
			case '<':
				return $expr->lt($key, $value);
			case '>':
				return $expr->gt($key, $value);
			case '!':
				return $expr->neq($key, $value);
			case '~':
				return $expr->contains($key, $value);
			case '=':
			default:
				return $expr->eq($key, $value);
		}
	}	
}