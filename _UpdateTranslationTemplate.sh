#!/bin/bash
find . -iname "*.php" -not -path "./vendor/*" | xargs \
	xgettext -o locale/messages.pot \
		--package-name="labo-admin" \
		--copyright-holder="Rolf Michael Bislin alias romibi (http://romibi.ch/)" \
		--msgid-bugs-address=info@romibi.ch \
		--from-code=UTF-8 -n