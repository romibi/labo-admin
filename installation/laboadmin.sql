SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `laboadmin`
--

-- --------------------------------------------------------

--
-- Table schema for `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table data for `config`
--

INSERT INTO `config` (`key`, `value`, `description`, `version`) VALUES
('general.addresses', '[\r\n	{\r\n		\"doctor\": \"Dr. Beispiel\",\r\n		\"text\": \"Dr Beispiel\\nSpezialist für irgendwas\\nStrasse ins Nichts -1\\n???? Nirgens\\nTel. 000 / 000 00 00\\nFax 000 / 000 00 01\\nMail : info@romibi.ch\"\r\n	},\r\n	{\r\n		\"doctor\": \"Dr Mad\",\r\n		\"text\": \"Dr Mad\\nSpezialist für alles Mögliche\\nInstitution für sonstwas\\nStrasse ins Nichts oo\\n???? Nirgens\\nTel.: 000 / 000 00 02\\nFax : 000 / 000 00 03\"\r\n	}\r\n]', NULL, 1),
('general.dateformat.file', 'Y-m-d', NULL, 1),
('general.dateformat.list', 'd.m.Y', NULL, 1),
('general.dateformat.text', 'd.m.Y', NULL, 1),
('general.language', 'de_CH', NULL, 1),
('general.pdf.filenameformat', '<CT> <patient> <pagename>', '<CT>=Current Timestamp\r\n<FT>=First Timestamp\r\n<LT>=Last Timestamp\r\n<pagename>=Pagename\r\n<patient>=Patient-Name', 1),
('general.projectitle', 'labo-admin', NULL, 1),
('general.textpreviewlength', '80', NULL, 1),
('layout.demo', 'false', NULL, 1),
('layout.footer.show', 'true', NULL, 1),
('layout.showGitVersion', 'false', NULL, 1),
('layout.testtype.print', '[\r\n	{\r\n		\"type\": \"header\"\r\n	},\r\n	{\r\n		\"type\": \"table\",\r\n		\"title\": \"SANG\",\r\n		\"elements\": [\r\n				{\"id\": \"vs\"},\r\n				{\"id\": \"crp\", \"borderBottomStyle\": \"double\"},\r\n				{\"id\": \"ht\"},\r\n				{\"id\": \"hb\"},\r\n				{\"id\": \"ery\", \"fontStyle\": \"bold\"},\r\n				{\"id\": \"mcv\"},\r\n				{\"id\": \"mch\"},\r\n				{\"id\": \"mchc\", \"borderBottomStyle\": \"solid\", \"borderBottomWidth\": 3},\r\n				{\"id\": \"leuc\", \"fontStyle\": \"bold\"},\r\n				{\"id\": \"nns\"},\r\n				{\"id\": \"ns\"},\r\n				{\"id\": \"eos\"},\r\n				{\"id\": \"baso\"},\r\n				{\"id\": \"mono\"},\r\n				{\"id\": \"lymph\"},\r\n				{\"type\": \"spacer\"},\r\n				{\"id\": \"thrb\", \"fontStyle\": \"bold\"},\r\n				{\"id\": \"tp\"},\r\n				{\"id\": \"billitot\"},\r\n				{\"id\": \"phalc\", \"fontStyle\": \"bold\"},\r\n				{\"id\": \"gotasat\"},\r\n				{\"id\": \"gptalat\"},\r\n				{\"id\": \"ggt\"},\r\n				{\"id\": \"amylase\", \"borderBottomStyle\": \"double\"},\r\n				{\"id\": \"creat\"},\r\n				{\"id\": \"uree\"},\r\n				{\"id\": \"acurique\"},\r\n				{\"id\": \"kplus\", \"borderBottomStyle\": \"double\"},\r\n				{\"id\": \"hba1c\"},\r\n				{\"id\": \"glucose\"},\r\n				{\"id\": \"triglyc\"},\r\n				{\"id\": \"choltot\"},\r\n				{\"id\": \"hdl\"},\r\n				{\"id\": \"choltothdl\", \"borderBottomStyle\": \"double\"},\r\n				{\"id\": \"ldl\"}\r\n			]\r\n	},\r\n	{\r\n		\"type\": \"pagebreak\"\r\n	},\r\n	{\r\n		\"type\": \"header\"\r\n	},\r\n	{\r\n		\"type\": \"table\",\r\n		\"title\": \"URINES\",\r\n		\"elements\": [\r\n				{\r\n					\"type\": \"label\",\r\n					\"text\": \"SEDIMENT:\",\r\n					\"fontStyle\": \"bold\"\r\n				},\r\n				{\"id\": \"u_leucos\"},\r\n				{\"id\": \"germes\"},\r\n				{\"id\": \"eryth\"},\r\n				{\"id\": \"cylindre\"},\r\n				{\"id\": \"cellepith\"},\r\n				{\"id\": \"cristaux\"},\r\n				{\"type\": \"spacer\", \"borderBottomStyle\": \"solid\", \"borderBottomWidth\": 3},\r\n				{\r\n					\"type\": \"label\",\r\n					\"text\": \"EXAMENS:\",\r\n					\"fontStyle\": \"bold\"\r\n				},\r\n				{\"id\": \"u_glucose\"},\r\n				{\"id\": \"bilirubine\"},\r\n				{\"id\": \"cetones\"},\r\n				{\"id\": \"densite\"},\r\n				{\"id\": \"sang\"},\r\n				{\"id\": \"ph\"},\r\n				{\"id\": \"proteines\"},\r\n				{\"id\": \"urobgene\"},\r\n				{\"id\": \"nitrites\"},\r\n				{\"id\": \"leucos\", \"borderBottomStyle\": \"double\"},\r\n				{\"id\": \"albumine\"},\r\n				{\"id\": \"creatinine\"},\r\n				{\"id\": \"albcreat\"},\r\n				{\"id\": \"uricult\"},\r\n				{\"type\": \"spacer\"}\r\n		]\r\n	},\r\n	{\r\n		\"title\": \"SELLES\",\r\n		\"type\": \"table\",\r\n		\"elements\": [\r\n				{\"id\": \"sangocculte\"}\r\n		],\r\n		\"hideDates\": true\r\n	},\r\n	{\r\n		\"title\": \"AUTRES EXAMENS\",\r\n		\"type\": \"table\",\r\n		\"elements\": [\r\n				{\"id\": \"strepta\"},\r\n				{\"type\": \"spacer\"}\r\n		],\r\n		\"hideDates\": true\r\n	}\r\n]', NULL, 1),
('layout.testtype.web', '[\r\n	{\r\n		\"title\": \"Sang\",\r\n		\"elements\": [\r\n			[\r\n				{\"id\": \"vs\"},\r\n				{\"id\": \"crp\"},\r\n				{\"id\": \"fss\", \"disables\": [\"fsc\"]},\r\n				{\"id\": \"fsc\", \"disables\": [\"fss\"]},\r\n				{\"id\": \"tp\"},\r\n				{\"type\": \"spacer\"},\r\n				{\"type\": \"spacer\"},\r\n				{\"id\": \"strepta\"},\r\n				{\"id\": \"sangocculte\"}\r\n			],\r\n			[\r\n				{\"id\": \"billitot\"},\r\n				{\"id\": \"phalc\"},\r\n				{\"id\": \"gotasat\"},\r\n				{\"id\": \"gptalat\"},\r\n				{\"id\": \"ggt\"},\r\n				{\"id\": \"amylase\"},\r\n				{\"id\": \"lipase\"},\r\n				{\r\n					\"id\": \"hepa1\",\r\n					\"name\": \"hepa1\",\r\n					\"type\": \"groupToggle\",\r\n					\"sets\": [\r\n						\"billitot\",\r\n						\"phalc\",\r\n						\"gotasat\",\r\n						\"gptalat\",\r\n						\"ggt\"\r\n					],\r\n					\"disables\": [\"hepa2\",\"hepa3\"]\r\n				},\r\n				{\r\n					\"id\": \"hepa2\",\r\n					\"name\": \"hepa2\",\r\n					\"type\": \"groupToggle\",\r\n					\"sets\": [\r\n						\"gotasat\",\r\n						\"gptalat\"\r\n					],\r\n					\"disables\": [\"hepa1\",\"hepa3\"]\r\n				},\r\n				{\r\n					\"id\": \"hepa3\",\r\n					\"name\": \"hepa3\",\r\n					\"type\": \"groupToggle\",\r\n					\"sets\": [\r\n						\"phalc\",\r\n						\"gotasat\",\r\n						\"gptalat\"\r\n					],\r\n					\"disables\": [\"hepa1\",\"hepa2\"]\r\n				}\r\n			],\r\n			[\r\n				{\"id\": \"creat\"},\r\n				{\"id\": \"uree\"},\r\n				{\"id\": \"acurique\"},\r\n				{\"id\": \"kplus\"},\r\n				{\"id\": \"ca\"},\r\n				{\"id\": \"p\"},\r\n				{\"type\": \"spacer\"},\r\n				{\"id\": \"psa\"},\r\n				{\"id\": \"psalibre\"},\r\n				{\"id\": \"tshft4\"},\r\n				{\"id\": \"ferritine\"},\r\n				{\"id\": \"acfol\"},\r\n				{\"id\": \"vitb12\"}\r\n			],\r\n			[\r\n				{\"id\": \"glucose\"},\r\n				{\"id\": \"triglyc\"},\r\n				{\"id\": \"choltot\"},\r\n				{\"id\": \"hdl\"},\r\n				{\"id\": \"choltothdl\"},\r\n				{\"id\": \"ldl\"},\r\n				{\r\n					\"id\": \"ligl\",\r\n					\"name\": \"Ligl\",\r\n					\"type\": \"groupToggle\",\r\n					\"sets\": [\r\n						\"glucose\",\r\n						\"triglyc\",\r\n						\"choltot\",\r\n						\"hdl\",\r\n						\"choltothdl\",\r\n						\"ldl\"\r\n					],\r\n					\"disables\": [\"lipide\"]\r\n				},\r\n				{\r\n					\"id\": \"lipide\",\r\n					\"name\": \"Lipide\",\r\n					\"type\": \"groupToggle\",\r\n					\"sets\": [\r\n						\"triglyc\",\r\n						\"choltot\",\r\n						\"hdl\",\r\n						\"choltothdl\",\r\n						\"ldl\"\r\n					],\r\n					\"disables\": [\"ligl\"]\r\n				},\r\n				{\"id\": \"hba1c\"}\r\n			]\r\n		]\r\n	},\r\n	{\r\n		\"title\": \"Urines\",\r\n		\"elements\": [\r\n			[\r\n				{\"id\": \"stu\", \"disables\": [\"stx\"]},\r\n				{\"id\": \"stx\", \"disables\": [\"stu\"]},\r\n				{\"id\": \"uricult\"},\r\n				{\"id\": \"microalbuminurie\"},\r\n				{\"id\": \"albcreat\", \"hideCheckbox\": true}\r\n			]\r\n		]\r\n	},\r\n	{\r\n		\"title\": \"Examens suppl\",\r\n		\"elements\": [\r\n			[\r\n				{\"id\": \"ecg\"},\r\n				{\"id\": \"rx\"}\r\n			]\r\n		]\r\n	}\r\n]', NULL, 1);

-- --------------------------------------------------------

--
-- Table schema for `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diabetes` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table schema for `samples`
--

CREATE TABLE IF NOT EXISTS `samples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `resultcomment` text COLLATE utf8mb4_unicode_ci,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `marked` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_patientId` (`patientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table schema for `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sampleId` int(11) NOT NULL,
  `testtypeId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testInstructions` text COLLATE utf8mb4_unicode_ci,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_sampleId` (`sampleId`),
  KEY `fk_testtypeId` (`testtypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table schema for `testtypes`
--

CREATE TABLE IF NOT EXISTS `testtypes` (
  `id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'test',
  `valueType` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'void',
  `instructions` tinyint(1) NOT NULL DEFAULT '0',
  `normals` mediumtext COLLATE utf8mb4_unicode_ci,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pointsSupplement` float NOT NULL DEFAULT '0',
  `pointsAnalyse` float NOT NULL DEFAULT '0',
  `contains` mediumtext COLLATE utf8mb4_unicode_ci,
  `containsNot` mediumtext COLLATE utf8mb4_unicode_ci,
  `depends` mediumtext COLLATE utf8mb4_unicode_ci,
  `formula` mediumtext COLLATE utf8mb4_unicode_ci,
  `round` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table data for `testtypes`
--

INSERT INTO `testtypes` (`id`, `name`, `type`, `valueType`, `instructions`, `normals`, `unit`, `pointsSupplement`, `pointsAnalyse`, `contains`, `containsNot`, `depends`, `formula`, `round`, `version`) VALUES
('acfol', 'acfol', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('acurique', 'Ac. ur.', 'test', 'int', 0, '{\"default\": {\"min\": 170, \"max\": 490}}', 'μmol/l', 3, 2.8, NULL, NULL, NULL, NULL, NULL, 1),
('albcreat', 'Alb/Créa', 'calculated', 'void', 0, '{\"default\": {\"max\": 16, \"comment\": \"(spontané)\"}}', 'mg/g', 0, 0, NULL, NULL, '[\"albumine\", \"creatinine\"]', 'albumine/(creatinine*10)', 2, 1),
('albumine', 'Albumin', 'test', 'int', 0, '{\"default\": {\"max\": 37, \"comment\": \"(spontané)\"}}', 'mg/l', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('amylase', 'Amylase', 'test', 'int', 0, '{\"default\": {\"min\": 30, \"max\": 110}}', 'U/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('baso', 'Basophiles', 'test', 'int', 0, '{\"default\": {\"min\": 0, \"max\": 1}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('bilirubine', 'Bilirubine', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('billitot', 'Billi. tot.', 'test', 'int', 0, '{\"default\": {\"min\": 3, \"max\": 22}}', 'μmol/l', 3, 3.2, NULL, NULL, NULL, NULL, NULL, 1),
('ca', 'ca', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('cellepith', 'Cell. Épith.', 'test', 'int', 0, '{\"default\": {\"text\": \"rares\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('cetones', 'Cétones', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('choltot', 'Chol. Tot.', 'test', 'int', 0, '{\"default\": {\"max\": 6.5}, \"diabetes\": {\"max\": 5}}', 'mmol/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('choltothdl', 'Chol.tot/HDL', 'calculated', 'void', 0, '{\"default\": {\"max\": 5}}', NULL, 0, 0, NULL, NULL, '[\"choltot\", \"hdl\"]', 'choltot/hdl', 2, 1),
('creat', 'Créat.', 'test', 'int', 0, '{\"default\": {\"min\": 44, \"max\": 124}}', 'μmol/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('creatinine', 'Créa', 'test', 'int', 0, '{\"default\": {\"min\": 36, \"max\": 500, \"comment\": \"(spontané)\"}}', 'mg/dl', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('cristaux', 'Cristaux', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('crp', 'CRP', 'test', 'int', 0, '{\"default\": {\"max\": 11}}', 'mg/l', 3, 9, NULL, NULL, NULL, NULL, NULL, 1),
('cylindre', 'Cylindres', 'test', 'int', 0, '{\"default\": {\"text\": \"absents\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('densite', 'Densité', 'test', 'int', 0, '{\"default\": {\"min\": 1000, \"max\": 1020}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ecg', 'ECG', 'test', 'str', 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('eos', 'Eosinophiles', 'test', 'int', 0, '{\"default\": {\"min\": 1, \"max\": 3}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ery', 'Ery', 'test', 'int', 0, '{\"default\": {\"min\": 4, \"max\": 5.5}}', 'x 10⁶/mm³', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('eryth', 'Erythros', 'test', 'int', 0, '{\"default\": {\"min\": 0, \"max\": 2}}', '/ champ', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ferritine', 'ferritine', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('fsc', 'FSC', 'group', 'void', 0, NULL, NULL, 4, 35, '[\"ht\", \"hb\", \"ery\", \"mcv\", \"mch\", \"mchc\", \"leuc\", \"thrb\", \"nns\", \"ns\", \"eos\", \"baso\", \"mono\", \"lymph\"]', NULL, NULL, NULL, NULL, 1),
('fss', 'FSS', 'group', 'void', 0, NULL, NULL, 2, 9, '[\"ht\", \"hb\", \"ery\", \"mcv\", \"mch\", \"mchc\", \"leuc\", \"thrb\"]', '[\"nns\", \"ns\", \"eos\", \"baso\", \"mono\", \"lymph\"]', NULL, NULL, NULL, 1),
('germes', 'Germes', 'test', 'int', 0, '{\"default\": {\"max\": 10}}', '/ champ', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ggt', 'γ-GT', 'test', 'int', 0, '{\"m\": {\"min\": 11,\"max\": 50}, \"f\": {\"min\": 7, \"max\": 32}}', 'U/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('glucose', 'Glucose', 'test', 'int', 0, '{\"default\": {\"min\": 3.9, \"max\": 6.0}}', 'mmol/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('gotasat', 'GOT / AST', 'test', 'int', 0, '{\"m\": {\"max\": 40}, \"f\": {\"max\": 33}}', 'U/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('gptalat', 'GPT / ALT', 'test', 'int', 0, '{\"m\": {\"max\": 41}, \"f\": {\"max\": 32}}', 'U/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('hb', 'Hb', 'test', 'int', 0, '{\"default\": {\"min\": 120, \"max\": 160}}', 'g/l', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('hba1c', 'HbA¹c', 'test', 'int', 0, '{\"default\": {\"max\": 6}}', '%', 3, 17.8, NULL, NULL, NULL, NULL, NULL, 1),
('hdl', 'Chol.HDL', 'test', 'int', 0, '{\"default\": {\"min\": 0.78, \"max\": 2.6}}', 'mmol/l', 3, 3.2, NULL, NULL, NULL, NULL, NULL, 1),
('ht', 'Ht', 'test', 'int', 0, '{\"default\": {\"min\": 38, \"max\": 50}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('kplus', 'K⁺', 'test', 'int', 0, '{\"default\": {\"min\": 3.5, \"max\": 4.6}}', 'mmol/l', 3, 2.8, NULL, NULL, NULL, NULL, NULL, 1),
('ldl', 'Chol.LDL', 'test', 'int', 0, '{\"default\": {\"max\": 3}, \"diabetes\": {\"max\": 2.6}}', 'mmol/l', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('leuc', 'Leuc', 'test', 'int', 0, '{\"default\": {\"min\": 4.5, \"max\": 9}}', 'x 10³/mm³', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('leucos', 'Leucos', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('lipase', 'lipase', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('lymph', 'Lympho', 'test', 'int', 0, '{\"default\": {\"min\": 25, \"max\": 33}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('mch', 'MCH', 'test', 'int', 0, '{\"default\": {\"min\": 25, \"max\": 35}}', 'pg', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('mchc', 'MCHC', 'test', 'int', 0, '{\"default\": {\"min\": 310, \"max\": 370}}', 'g/l', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('mcv', 'MCV', 'test', 'int', 0, '{\"default\": {\"min\": 80, \"max\": 100}}', 'fl', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('microalbuminurie', 'Microalbuminurie', 'group', 'void', 0, NULL, NULL, 3, 11.2, '[\"albumine\", \"creatinine\"]', NULL, NULL, NULL, NULL, 1),
('mono', 'Monocytes', 'test', 'int', 0, '{\"default\": {\"min\": 3, \"max\": 7}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('nitrites', 'Nitrites', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('nns', 'Neutro ns', 'test', 'int', 0, '{\"default\": {\"min\": 3, \"max\": 5}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ns', 'Neutro s', 'test', 'int', 0, '{\"default\": {\"min\": 54, \"max\": 62}}', '%', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('p', 'p', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('ph', 'pH', 'test', 'int', 0, '{\"default\": {\"min\": 4.5, \"max\": 5.5}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('phalc', 'Phosph. alc.', 'test', 'int', 0, '{\"m\": {\"max\": 129}, \"f\": {\"max\": 104}}', 'U/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('proteines', 'Proteïnes', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('psa', 'psa', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('psalibre', 'psalibre', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('rx', 'rx', 'test', 'str', 1, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('sang', 'Sang', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('sangocculte', 'Sang occulte', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 9, 27.9, NULL, NULL, NULL, NULL, NULL, 1),
('strepta', 'Streptos gorge', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 2, 14.8, NULL, NULL, NULL, NULL, NULL, 1),
('stu', 'STU', 'group', 'void', 0, NULL, NULL, 3, 20, '[\"u_leucos\", \"germes\", \"eryth\", \"cylindre\", \"cellepith\", \"cristaux\", \"u_glucose\", \"bilirubine\", \"cetones\", \"densite\", \"sang\", \"ph\", \"proteines\", \"urobgene\", \"nitrites\", \"leucos\"]', NULL, NULL, NULL, NULL, 1),
('stx', 'STX', 'group', 'void', 0, NULL, NULL, 3, 1, '[\"u_glucose\", \"bilirubine\", \"cetones\", \"densite\", \"sang\", \"ph\", \"proteines\", \"urobgene\", \"nitrites\", \"leucos\"]', '[\"u_leucos\", \"germes\", \"eryth\", \"cylindre\", \"cellepith\"]', NULL, NULL, NULL, 1),
('thrb', 'Thrb', 'test', 'int', 0, '{\"default\": {\"min\": 150, \"max\": 400}}', 'x 10³/mm³', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('tp', 'TP INR', 'test', 'int', 0, '{\"default\": {\"min\": 2.0, \"max\": 4.5, \"comment\": \"thérapeutique\"}}', '', 2, 6, NULL, NULL, NULL, NULL, NULL, 1),
('triglyc', 'Trigly.', 'test', 'int', 0, '{\"default\": {\"min\": 0.4, \"max\": 1.7}}', 'mmol/l', 3, 2.8, NULL, NULL, NULL, NULL, NULL, 1),
('tshft4', 'tshft4', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('u_glucose', 'Glucose', 'test', 'int', 0, '{\"default\": {\"text\": \"négatif\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('u_leucos', 'Leucos', 'test', 'int', 0, '{\"default\": {\"min\": 0, \"max\": 2}}', '/ champ', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('uree', 'Urée', 'test', 'int', 0, '{\"default\": {\"min\": 2.5, \"max\": 6.43}}', 'mmol/l', 3, 2.5, NULL, NULL, NULL, NULL, NULL, 1),
('uricult', 'Uricult', 'test', 'int', 0, '{\"default\": {\"max\": 100000}}', 'germes', 2, 9.3, NULL, NULL, NULL, NULL, NULL, 1),
('urobgene', 'Uro-b-gène', 'test', 'int', 0, '{\"default\": {\"text\": \"traces\"}}', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('vitb12', 'vitb12', 'test', 'int', 0, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 1),
('vs', 'VS', 'test', 'int', 0, '{\"default\": {\"min\": 0, \"max\": 15}}', 'mm/h', 3, 1, NULL, NULL, NULL, NULL, NULL, 1);

--
-- Constraints of Tables
--

--
-- Constraints of `samples`
--
ALTER TABLE `samples`
  ADD CONSTRAINT `fk_patientId` FOREIGN KEY (`patientId`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints of `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `fk_sampleId` FOREIGN KEY (`sampleId`) REFERENCES `samples` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_testtypeId` FOREIGN KEY (`testtypeId`) REFERENCES `testtypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;