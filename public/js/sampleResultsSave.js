// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
var sampleResultsSave = function (returnPath, successAction, errorMsg) {
	if($('#saveButton').hasClass('disabled')) return;
	$('#saveButton').addClass('disabled');
	$('#submitButton').addClass('disabled');

	var sample = {};

	var sampleId = $('input[name="sampleId"]').val();
	var patientId = $('input[name="patientId"]').val();

	var resultcommentObj = $('textarea[name="resultcomment"]');
	var testtypevalues = $('input.testtype');
	
	var status = {'runningTasks': 0, 'errors': []};
	status.newTask = function () {
		this.runningTasks++;
	};

	status.taskFinished = function () {
		this.runningTasks--;
		if(this.runningTasks==0) {
			var msg = "";
			for (var i = this.errors.length - 1; i >= 0; i--) {
				msg += this.errors[i] + '<br>';
			}
			if(msg=="") {
				if(typeof successAction != "undefined") {
					successAction();
				} else {
					// TODO: show nicer success popup
					alert('success');
					$('#saveButton').removeClass('disabled');
					$('#submitButton').removeClass('disabled');
				}
			} else {
				if(typeof errorMsg != "undefined") {
					msg += errorMsg+"<br>";
				}
				$('#saveFailedPopup .modal-body p').html(msg);
				$('#saveFailedPopup').modal()
			}
		}
	}

	status.newTask();
	var somethingToSave = false;

	var sample = {'id': sampleId};
	
	if(resultcommentObj.val()!=resultcommentObj.attr('data-value-unchanged')) {
		sample['resultcomment'] = resultcommentObj.val();
		somethingToSave = true;
	}

	sample['_version']=$('input[name="version"]').val();
	if(somethingToSave) {
		status.runningTasks++;
		$.ajax({ 
			type: "PATCH",
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/sample/"+sampleId,
			data: JSON.stringify(sample),
			success: function(data){
				$('input[name="version"]').val(data['_version']);
				status.taskFinished()
			},
			error: function(xhr, textStatus, thrownError) {
				status.errors.push(getErrorFromXHR(xhr))
				status.taskFinished();
			}
		});
	}

	testtypevalues.each(function() {
		var value = $(this).val();
		var orig = $(this).attr('data-value-unchanged');
		if(value!=orig) {
			var test = {'testtypeId': $(this).attr('id')};
			test['value']=value;
			test["_version"] = $("#"+test.testtypeId+"_version").val();
			status.newTask();
			updateTest(sampleId, test, status);
		}
	});
	status.taskFinished();
}

var sampleResultsSubmit = function(returnPath, errorMsg) {
	if($('#submitButton').hasClass('disabled')) return;
	$('#submitButton').addClass('disabled');
	
	sampleResultsSave(returnPath, function() {
		var patientId = $('input[name="patientId"]').val();
		var sampleId = $('input[name="sampleId"]').val();
		var sample = {'id': sampleId,
			'done': true,
			'_version': $('input[name="version"]').val()
		};

		$.ajax({ 
			type: "PATCH",
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/sample/"+sampleId,
			data: JSON.stringify(sample),
			success: function(data){
				window.location.href = returnPath;
			},
			error: function(xhr, textStatus, thrownError) {
				$('#saveFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
				$('#saveFailedPopup').modal()
			}
		});
	}, errorMsg);
}

var updateTest = function (sampleId, test, status) {
	$.ajax({
		type: "PATCH",
		contentType : 'application/json',
		dataType: "json",
		url: "/api/sample/"+sampleId+"/test/"+test.testtypeId,
		data: JSON.stringify(test),
		success: function(data) {
			$('#'+test.testtypeId+'_version').val(data['_version']);
			$('#'+test.testtypeId).attr('data-value-unchanged', data.value);
			status.taskFinished();
		},
		error: function(xhr, textStatus, thrownError) {
			status.errors.push(getErrorFromXHR(xhr))
			status.taskFinished();
		}
	});
}

