<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;

require_once 'entities/Patient.php';
require_once 'entities/Sample.php';
require_once 'entities/Test.php';
require_once 'entities/TestType.php';
require_once 'entities/Config.php';

require_once 'controllers/PatientController.php';
require_once 'controllers/SampleController.php';
require_once 'controllers/TestController.php';
require_once 'controllers/TestTypeController.php';
require_once 'controllers/ConfigController.php';

require_once 'web/WebController.php';

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Dflydev\FigCookies\Cookies;
use Dflydev\FigCookies\SetCookie;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;


class LaboAdmin {
	private static $instances = array();
	protected $app;
	protected $request;
	protected $response;
	protected $entityManager;
	protected $controllers;
	protected $web;
	protected $initialized = false;
	protected $config = array();

	private function __construct() {
		$this->web = new WebController();
		$this->tryInitialize();
	}

	public static function getInstance($key='') {
		if(isset(self::$instances[$key])) {
			return self::$instances[$key];
		}
		self::$instances[$key] = new LaboAdmin();
		return self::$instances[$key];
	}

	public function setSlimAppObj($app) {
		$this->app = $app;
		$this->tryInitialize();
	}

	public function setBaseConfig($config) {
		$this->config = $config;
	}

	public function setEntityManager($entityManager) {
		$this->entityManager = $entityManager;
		$this->tryInitialize();
	}

	public function setRequestObj($req) {
		$this->request = $req;
		$this->tryInitialize();
	}

	public function setResponseObj($resp) {
		$this->response = $resp;
		$this->tryInitialize();
	}

	public function getBaseConfig() {
		return $this->config;
	}

	public function getCookie($key, $default=null) {
		if($default==null)
			return FigRequestCookies::get($this->request, $key)->getValue();
		else
			return FigRequestCookies::get($this->request, $key, $default)->getValue();
	}

	public function setCookie($key, $value, $expiration=null) {
		$cookie =  SetCookie::create($key)
						->withValue($value)
						->withPath('/');
		if($expiration==null) {
			$cookie = $cookie->rememberForever();
		} else {
			$cookie = $cookie->withExpires($expiration);
		}
		$response = FigResponseCookies::set($this->response, $cookie);
		$this->setResponseObj($response);
		return $response;
	}

	private function tryInitialize() {
		if($this->initialized) return;
		if(isset($this->app) && isset($this->request) && isset($this->response) && isset($this->entityManager)) {
			$this->initialize();
		}
	}

	private function initialize() {
		$this->initTableNamingOverride();
		$this->controllers['patient'] = new PatientController($this->entityManager);
		$this->controllers['sample'] = new SampleController($this->entityManager);
		$this->controllers['test'] = new TestController($this->entityManager);
		$this->controllers['testtype'] = new TestTypeController($this->entityManager);
		$this->controllers['config'] = new ConfigController($this->entityManager);

		$this->initialized = true;
	}

	public function web() {
		return $this->web;
	}

	private function initTableNamingOverride() {
		$dbBaseConf = $this->getBaseConfig()['custom']['db'];
		$this->tableNamingOverride('ch\romibi\labo_admin\Patient', $dbBaseConf);
		$this->tableNamingOverride('ch\romibi\labo_admin\Sample', $dbBaseConf);
		$this->tableNamingOverride('ch\romibi\labo_admin\Test', $dbBaseConf);
		$this->tableNamingOverride('ch\romibi\labo_admin\TestType', $dbBaseConf);
		$this->tableNamingOverride('ch\romibi\labo_admin\Config', $dbBaseConf);
	}

	private function tableNamingOverride($entityClass, $dbconf) {
		$meta = $this->entityManager->getClassMetadata($entityClass);
		$defaultTableName = $this->tableNamePrefixing($meta, $dbconf);

		if(isset($dbconf['namingOverride'])) {
			$overrides = $dbconf['namingOverride'];
			$this->tableNameOverride($meta, $overrides, $defaultTableName);
			$this->tableFieldNameOverrides($meta, $overrides, $defaultTableName);
		}
	}

	private function tableNamePrefixing($meta, $dbconf) {
		$defaultName = $meta->getTableName();

		$prefix = '';
		if(isset($dbconf['prefix']) && !is_array($dbconf['prefix'])) {
			$prefix = $dbconf['prefix'];
		}
		if(strlen($prefix)>0) {
			$meta->setTableName($prefix.$defaultName);
		}
		return $defaultName;
	}

	private function tableNameOverride($meta, $overrides, $defaultTableName) {
		$tableOverride = '';
		if(isset($overrides['tables']) && isset($overrides['tables'][$defaultTableName])
				&& !is_array($overrides['tables'][$defaultTableName])) {
			$tableOverride = $overrides['tables'][$defaultTableName]; 
		}
		if(strlen($tableOverride)>0) {
			$meta->setTableName($tableOverride);
		}
	}

	private function tableFieldNameOverrides($meta, $overrides, $defaultTableName) {
		if(isset($overrides[$defaultTableName.'Columns']) && is_array($overrides[$defaultTableName.'Columns'])) {
			foreach ($overrides[$defaultTableName.'Columns'] as $key => $value) {
				if(isset($key) && !is_array($key) && isset($value) && !is_array($value)) {
					$meta->fieldMappings[$key]['columnName']=$value;
				}
			}
		}
	}

	public function __call($name, $args) {
		if(!$this->initialized) { throw new \Exception('labo_admin isn\'t finished initializing...'); }
		return $this->controllers[$name];
	}
}

class EntityChangedException extends \Exception {}