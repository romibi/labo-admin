<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use ch\romibi\labo_admin;
use ch\romibi\labo_admin\ApiHelper;

/* Api-routes */
$app->group('/api', function () use ($app, $laboadmin) {
	/* patient */
	$app->group('/patient', function () use ($app, $laboadmin) {
		$app->get('/search/{key}', function ($request, $response, $args) use ($app, $laboadmin) {
			echo ApiHelper::getInstance()->prepare(array('patients'=>$laboadmin->patient()->search($args['key'])));
		});

		$app->map(['GET', 'PUT', 'PATCH', 'DELETE'], '/{id}', function ($request, $response, $args) use ($app, $laboadmin) {
			if ($request->isGet()) {
				echo $laboadmin->patient()->get($args['id']);
			}
			if($request->isPut()) {
				echo $laboadmin->patient()->put($args['id'], $request->getParsedBody());
			}
			if($request->isPatch()) {
				try{
					echo $laboadmin->patient()->patch($args['id'], $request->getParsedBody());
				} catch(\ch\romibi\labo_admin\EntityChangedException $e) {
					echo ApiHelper::getInstance()->error($e);
				}
			}
			if($request->isDelete()) {
				echo $laboadmin->patient()->delete($args['id']);
			}
		});

		/* new sample for patient */
		$app->get('/{patientId}/samples', function ($request, $response, $args) use ($app, $laboadmin) {
			echo ApiHelper::getInstance()->prepare($laboadmin->patient()->getSamples($args['patientId']));
		});
		$app->map(['GET','POST'], '/{patientId}/sample', function ($request, $response, $args) use ($app, $laboadmin) {
			if($request->isGet()){
				echo ApiHelper::getInstance()->prepare($laboadmin->patient()->getSamples($args['patientId']));
			}
			if($request->isPost()){
				echo $laboadmin->patient()->postSample($args['patientId'], $request->getParsedBody());
			}
		});
	});

	/* sample */
	$app->group('/sample', function () use ($app, $laboadmin) {
		$app->get('/list', function($request, $response, $args) use ($app, $laboadmin) {
			echo ApiHelper::getInstance()->prepare(
				array('samples'=> $laboadmin->sample()->list($request->getQueryParams()) )
			);
		});
		$app->get('/count', function($request, $response, $args) use ($app, $laboadmin) {
			echo ApiHelper::getInstance()->prepare(array('count'=>$laboadmin->sample()->count($request->getQueryParams())));
		});
		$app->map(['GET', 'PATCH', 'DELETE'], '/{id:\d+}', function ($request, $response, $args) use ($app, $laboadmin) {
			if($request->isGet()) {
				echo $laboadmin->sample()->get($args['id']);
			}
			if($request->isPatch()) {
				try{
					echo $laboadmin->sample()->patch($args['id'], $request->getParsedBody());
				} catch(\ch\romibi\labo_admin\EntityChangedException $e) {
					echo ApiHelper::getInstance()->error($e);
				}
			}
			if($request->isDelete()) {
				echo $laboadmin->sample()->delete($args['id']);
			}
		});
	/* test */
		$app->put('/{sampleid:\d+}/test', function ($request, $response, $args) use ($app, $laboadmin) {
			echo $laboadmin->sample()->putTests($args['sampleid'], $request->getParsedBody());
		});
		$app->map(['GET', 'PUT', 'PATCH', 'DELETE'], '/{sampleid:\d+}/test/{testtype}', function ($request, $response, $args) use ($app, $laboadmin) {
			if($request->isGet()) {
				echo $laboadmin->test()->get($args['sampleid'], $args['testtype']);
			}
			if($request->isPut()) {
				$result = $laboadmin->test()->put($args['sampleid'], $args['testtype'], $request->getParsedBody());
				if(is_array($result)) {
					echo ApiHelper::getInstance()->prepare($result);
				} else {
					echo $result;
				}
			}
			if($request->isPatch()) {
				try{
					echo $laboadmin->test()->patch($args['sampleid'], $args['testtype'], $request->getParsedBody());
				} catch(\ch\romibi\labo_admin\EntityChangedException $e) {
					echo ApiHelper::getInstance()->error($e);
				}
			}
			if($request->isDelete()) {
				echo $laboadmin->test()->delete($args['sampleid'], $args['testtype']);
			}
		});
	});

	/* test-Types */
	$app->group('/testtype', function () use ($app, $laboadmin){
		$app->get('/', function($request, $response) use ($app, $laboadmin) {
			echo ApiHelper::getInstance()->prepare($laboadmin->testtype()->list());
		});
		$app->map(['GET', 'PUT', 'PATCH', 'DELETE'], '/{id}', function($request, $response, $args) use ($app, $laboadmin) {
			if($request->isGet()) {
				echo $laboadmin->testtype()->get($args['id']);
			}
			if($request->isPut()) {
				echo $laboadmin->testtype()->put($args['id'], $request->getParsedBody());
			}
			if($request->isPatch()) {
				try{
					echo $laboadmin->testtype()->patch($args['id'], $request->getParsedBody());
				} catch(\ch\romibi\labo_admin\EntityChangedException $e) {
					echo ApiHelper::getInstance()->error($e);
				}
			}
			if($request->isDelete()) {
				echo $laboadmin->testtype()->delete($args['id']);
			}
		});
		// cache?
	});

	$app->group('/user', function () use ($app, $laboadmin) {
		$app->map(['GET', 'POST', 'PATCH', 'DELETE'], '/{id}', function ($request, $response, $args) use ($app, $laboadmin) {
			echo 'not implemented yet';
		});
	});
})->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) use ($app, $laboadmin) {
    // Use the PSR 7 $request object
    $laboadmin->setRequestObj($request);
    $laboadmin->setResponseObj($response);
    ApiHelper::getInstance()->setAcceptType($request->getHeaders()['HTTP_ACCEPT'][0]);

    $response = $next($request, $response);
    
    $response = ApiHelper::getInstance()->getUpdatedResponse($response);
    return $response;
});