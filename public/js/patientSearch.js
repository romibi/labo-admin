// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
function searchPatients(event) {
	var searchObj = $('#patientSearch');
	var search = searchObj.val();
	var lastSearch = searchObj.attr('data-last-search');
	var lastPatientId = searchObj.attr('data-patient-id');
	if(search.length>2 && search!=lastSearch) {
		searchObj.attr('data-last-search', search);
		$.ajax({ 
			type: "GET",
			dataType: "json",
			url: "/api/patient/search/"+search,
			success: function(data){
				var patients = data.patients;
				if(patients.length==1) {
					searchObj.attr('data-patient-id', patients[0].id);
				} else {
					searchObj.attr('data-patient-id', '');
				}
				if(patients.length==0) {
					$('#patientSearchList tbody').html(render('patientSearchListEmpty',{}));
					return;
				}
				var list = "";
				for (var i = patients.length - 1; i >= 0; i--) {
					patient = patients[i];
					if(patient.gender == "m") {
						patient.gender = render('patientInfoGenderMale', {});
					}
					if(patient.gender == "f") {
						patient.gender = render('patientInfoGenderFemale', {});
					}
					if(patient.diabetes) {
						patient.diabetes = render('patientInfoHasDiabetes', {});
					} else {
						patient.diabetes = render('patientInfoHasNotDiabetes', {});
					}
					list += render('patientSearchListItem', patient);
				}
				$('#patientSearchList tbody').html(list);
			}
		});
	} else if(event.keyCode==13 && lastPatientId!='') {
		window.location.href = '/patient/'+lastPatientId;
	}
}