<?php

// replace with file to your own project bootstrap
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/labo-admin/bootstrap.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;


// replace with mechanism to retrieve EntityManager in your app
//$entityManager = GetEntityManager();

return ConsoleRunner::createHelperSet($entityManager);