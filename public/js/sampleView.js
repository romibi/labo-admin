// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
function sampleMark(returnPath, key, value) {
	$('#seenButton').addClass('disabled');
	$('#markedButton').addClass('disabled');

	var patientId = $('input[name="patientId"]').val();
	var sampleId = $('input[name="sampleId"]').val();
	var sample = {'id': sampleId,
		'_version': $('input[name="version"]').val()
	};
	sample[key] = value;

	$.ajax({ 
		type: "PATCH",
	    contentType : 'application/json',
		dataType: "json",
		url: "/api/sample/"+sampleId,
		data: JSON.stringify(sample),
		success: function(data){
			if(returnPath) {
				window.location.href = returnPath;
			} else {
				$('input[name="version"]').val(data['_version']);
				$('#seenButton').removeClass('disabled');
				$('#markedButton').removeClass('disabled');
			}
		},
		error: function(xhr, textStatus, thrownError) {
			$('#saveFailedPopup .modal-body p').html(getErrorFromXHR(xhr));
			$('#saveFailedPopup').modal()
		}
	});
}

function sampleMarkSeen(returnPath) {
	if($('#seenButton').hasClass('disabled')) return;
	sampleMark(returnPath, 'seen', true);
}

function sampleMarkMarked() {
	if($('#markedButton').hasClass('disabled')) return false;
	var marked = $('#marked').prop('checked');
	sampleMark(false, 'marked', marked);
}