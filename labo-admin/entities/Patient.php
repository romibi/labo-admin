<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractEntity.php';
/**
* @Entity @Table(name="patients", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
**/
class Patient extends AbstractEntity implements \JsonSerializable {
	/** @Id @Column(type="string") **/
	protected $id;
	/** @Version @Column(type="integer") */
    private $version = 1;
	/** @Column(type="string") **/
	protected $name;
	/** @Column(type="date") **/
	protected $birthdate;
	/** @Column(type="string", length=1) **/
	protected $gender;
	/** @Column(type="boolean") **/
	protected $diabetes;

	/** @OneToMany(targetEntity="Sample", mappedBy="patient") **/
    private $samples;

	public function __construct($id) {
		$this->id = $id;
		$this->samples = new ArrayCollection();
	}

	public static function normalizedFromArray($array, $setDefaults=false) {
		if(isset($array['id'])) { $ret['id'] = $array['id']; }
		if(isset($array['name'])) { $ret['name'] = $array['name']; }
		if(isset($array['birthdate']) && $array['birthdate']!="") { $ret['birthdate'] = new \DateTime($array['birthdate']); }
		if(isset($array['gender'])) { $ret['gender'] = $array['gender']; }
		if(isset($array['diabetes'])) { $ret['diabetes'] = ($array['diabetes'] == true); }
		else if($setDefaults) { $ret['diabetes'] = false; }
		//TODO: validate more?
		return $ret;
	}

	public function getId() {
		return $this->id;
	}

	public function getVersion() {
		return $this->version;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getBirthdate() {
		return $this->birthdate;
	}

	public function setBirthdate($date) {
		$this->birthdate = $date;
	}

	public function getAge($unit="h") {
		$now = new \DateTime();
		$interval = $now->diff($this->getBirthdate());
		switch ($unit) {
			case 'y':
				return $interval->y;
			case 'm':
				return ($interval->y * 12) + $interval->m;
			case 'd':
				return $interval->days;
			case 'h':
				return ($interval->days * 24) + $interval->h;
			default:
				return 0;
		}
	}

	public function getGender() {
		return $this->gender;
	}

	public function setGender($gender) {
		$this->gender = $gender;
	}

	public function hasDiabetes() {
		return $this->diabetes;
	}

	public function setHasDiabetes($diabetes) {
		$this->diabetes = $diabetes;
	}

	public function getSamples() {
		return $this->samples;
	}

	public function JsonSerialize()
	{
		$birthdate=null;
		if($this->birthdate)
			$birthdate = date_format($this->birthdate,'Y-m-d');
		return array('id'=>$this->id,
			'name'=>$this->name,
			'birthdate'=>$birthdate,
			'gender'=>$this->gender,
			'diabetes'=>$this->diabetes,
			'_version'=>$this->version
		);
	}
}
?>