// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
$(document).ready(function() {
	$("input[data-normals]").change(function() {
		checkNormals(this);
	});
	$("input[data-normals]").each(function (){
		checkNormals(this);
	});
});

function checkNormals(obj) {
	var value = $(obj).val();
	var normals = $.parseJSON($(obj).attr('data-normals'));

	var formgroup = $(obj).parentsUntil('.row','.form-group');

	if(value=="") {
		formgroup.removeClass('has-success');
		formgroup.removeClass('has-warning');
	} else if((typeof normals.min != "undefined" && value<normals.min) || 
		(typeof normals.max != "undefined" && value>normals.max)) {
		formgroup.removeClass('has-success');
		formgroup.addClass('has-warning');
	} else {
		formgroup.removeClass('has-warning');
		formgroup.addClass('has-success');
	}
}