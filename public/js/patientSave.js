// Copyright (c) 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
function patientSave() {
	if($('#saveButton').hasClass('disabled')) return;
	$('#saveButton').addClass('disabled');

	var id = $('input[name="id"]').val();
	var isNew = $('input[name="id"]').attr('data-isNew');
	var nameObj = $('input[name="name"]');
	var birthdateObj = $('input[name="birthdate"]');
	var genderObj = $('select[name="gender"]');
	var diabetesObj = $('input[name="diabetes"]');

	var patient = {};
	var method = "PATCH";

	var somethingToSave = false;

	if(isNew=="false") {
		var patient = {'id': id};
		if(nameObj.val()!=nameObj.attr('data-value-unchanged')) {
			patient['name']=nameObj.val();
			somethingToSave = true;
		}
		if(birthdateObj.val()!=birthdateObj.attr('data-value-unchanged')) {
			patient['birthdate']=birthdateObj.val();
			somethingToSave = true;
		}
		if(genderObj.val()!=genderObj.attr('data-value-unchanged')) {
			patient['gender']=genderObj.val();
			somethingToSave = true;
		}
		if(diabetesObj.prop('checked')+""!=diabetesObj.attr('data-value-unchanged')) {
			patient['diabetes']=diabetesObj.prop('checked');
			somethingToSave = true;
		}
		patient['_version']=$('input[name="version"]').val();
	} else {
		somethingToSave = true;
		method = "PUT";
		patient['name']=nameObj.val();
		patient['birthdate']=birthdateObj.val();
		patient['gender']=genderObj.val();
		patient['diabetes']=diabetesObj.prop('checked');
	}

	if(somethingToSave) {
		$.ajax({ 
			type: method,
		    contentType : 'application/json',
			dataType: "json",
			url: "/api/patient/"+id,
			data: JSON.stringify(patient),
			success: function(data){
				window.location.href = '/patient/'+data.id;
			},
			error: function(xhr, textStatus, thrownError) {
				messageobj = xhr.responseJSON;
				$('#saveFailedPopup .modal-body p').html(messageobj.message);
				$('#saveFailedPopup').modal()
			}
		});
	} else {
		window.location.href = '/patient/'+id;
	}
}