<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractController.php';
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class TestTypeController extends AbstractController {
	protected $entityManager;
	protected $repository;

	public function __construct($entityManager) {
		$this->entityManager = $entityManager;
		$this->repository = $entityManager->getRepository('ch\romibi\labo_admin\TestType');
	}

	public function list() {
		return $this->repository->findBy(array());
	}

	public function listById() {
		$list = $this->list();
		foreach ($list as $key => $value) {
			$list[$value->getId()] = $value;
			unset($list[$key]);
		}
		return $list;
	}

	public function get($id) {
		return $this->repository->find($id);
	}

	public function put($id, $object) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if(strtolower($id)!= strtolower($object['id'])) { throw new \Exception('multiple different ids provided'); }
		$newTtype = TestType::fromArray($object);
		$this->entityManager->persist($newTtype);
		$this->entityManager->flush();
		return $newTtype;
	}

	public function patch($id, $object) {
		if(!isset($object['id'])) { $object['id']=$id; }
		if(strtolower($id)!= strtolower($object['id'])) { throw new \Exception('multiple different ids provided'); }
		if(!isset($object['_version'])) { $object['_version']=-1; }
		try {
			$ttype = $this->get($id, LockMode::OPTIMISTIC, $object['_version']);
			$ttype->updateFromArray($object);
			$this->entityManager->flush();
			return $ttype;
		} catch(OptimisticLockException $e) {
			throw new EntityChangedException(_("Someone already changed something in this testtype"));
		}
	}

	public function delete($id) {
		$ttype = $this->get($id);
		$this->entityManager->remove($ttype);
		$this->entityManager->flush();
		return true;
	}
}
?>