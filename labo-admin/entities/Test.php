<?php
// Copyright (c) 2016 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
require_once 'AbstractEntity.php';
require_once __DIR__.'/../laboadmin.php';
use ch\romibi\labo_admin\LaboAdmin;
use jlawrence\eos\Parser;
/**
* @Entity @Table(name="tests", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
**/
class Test extends AbstractEntity implements \JsonSerializable {
	/** @Id @Column(type="integer") @GeneratedValue **/
	protected $id;
	/** @Version @Column(type="integer") */
    private $version;
	/** @Column(type="string") **/
	protected $testInstructions;
	/** @Column(type="string") **/
	private $value;

	/**
	 * @ManyToOne(targetEntity="Sample", inversedBy="tests") 
	 * @JoinColumn(name="sampleId", referencedColumnName="id")
	 */
    protected $sample;
	/** 
	 * @ManyToOne(targetEntity="TestType")
	 * @JoinColumn(name="testtypeId", referencedColumnName="id")
	 */
    protected $testtype;

    public function __construct($sample, $testtype) {
    	//TODO: check if test of testtype already exists
    	$this->sample = $sample;
    	$this->testtype = $testtype;
	}

	public static function normalizedFromArray($array, $setDefaults=false) {
		if(isset($array['testInstructions'])) { $ret['testInstructions'] = $array['testInstructions']; }
		if(isset($array['value'])) { $ret['value'] = $array['value']; }
		if(isset($array['sampleId'])) { $ret['sample'] = LaboAdmin::getInstance()->sample()->get($array['sampleId']); }
		if(isset($array['testtypeId'])) { $ret['testtype'] = LaboAdmin::getInstance()->testtype()->get($array['testtypeId']); }
		//TODO: validate more?
		return $ret;
	}

	public function getId() {
		return $this->id;
	}

	public function getVersion() {
		return $this->version;
	}

	public function getInstructions() {
		return $this->testInstructions;
	}

	public function getValue() {
		return $this->value;
	}

	public function setInstructions($instructions) {
		if(!$this->testtype->canHaveInstructions()) {
			throw new Exception("TestType isn't allowed to have instructions");
		}
		$this->testInstructions = $instructions;
	}

	public function setValue($value) {
		if(!$this->getTestType()->validate($value)) { throw new \Exception('invalid value for TestType'); }
		$this->value = $value;
	}

	public function getSample() {
		return $this->sample;
	}

	public function getTestType() {
		return $this->testtype;
	}

	public function isValueNormal() {
		return getTestType()->isValueNormal(getValue(), getSample()->getPatient()->getGender());
	}

	public function __set ( string $name , $value) {
		if($name == 'value') {
			$this->setValue($value);
		}
	}

	public function JsonSerialize()
	{
		$ret = array(
			//'id'=>$this->id, //not exposing this ... for now
			'value'=>$this->value,
			'testtypeId'=>$this->testtype->getId(),
			'_version'=>$this->version,
			'_embedded'=>array(
				'TestType'=>$this->testtype
				)
		);
		if($this->testtype->canHaveInstructions()) {
			$ret = array_merge(array('testInstructions' => $this->testInstructions),$ret);
		}
		return $ret;
	}
}