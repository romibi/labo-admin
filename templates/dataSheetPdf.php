<?php
// Copyright (c) 2014 - 2018 Rolf Michael Bislin. Licensed under the MIT license (see LICENSE.txt).
namespace ch\romibi\labo_admin;
use jlawrence\eos\Parser;

function printRowsToPDF($pdf, $data, $widths, $border, $align, $fontsize, $fontstyle, &$fill) {
  foreach($data as $row)
  {
    printRowToPDF($pdf, $row, $widths, $border, $align, $fontsize, $fontstyle, $fill);
  }
}

function printRowToPDF($pdf, $row, $widths, $border, $align, $fontsize, $fontstyle, &$fill) {
  $i = 0;
  foreach($row as $col) {
    $pdf->setFont('',$fontstyle[$i],$fontsize[$i]);
    $pdf->Cell($widths[$i],6,$col,$border[$i],0,$align[$i], $fill);
    $i++;
  }
  $fill = !$fill;
  $pdf->Ln();
}

function printHeader($pdf, $doc, $patient, $dateformat) {
  $pdf->setFont('','',10);

  $pdf->Write(5, str_replace('<br/>', '', str_replace('<br />', '', html_entity_decode($doc))));

  $pdf->SetLeftMargin(100);

  $pdf->Ln(-30);
  $pdf->setFont('','',12);
  $pdf->Write(5, _('Patient:').' '.$patient->getName());

  $pdf->SetLeftMargin(160);
  $pdf->Ln(0);
  $pdf->setFont('','B',12);
  $pdf->Write(5, _('No:').' ');
  $pdf->setFont('','',12);
  $pdf->Write(5, $patient->getId());

  $pdf->SetLeftMargin(100);
  
  $pdf->Ln(10);
  
  $pdf->Write(5, _("Birthdate").': '.$patient->getBirthdate()->format($dateformat));
  
  $pdf->ln(15);

  $pdf->setFont('','UB',14);
  $pdf->Write(5, strtoupper(_('Labor tests')));

  $pdf->Ln();

  $pdf->SetLeftMargin(10);

  $pdf->Ln(6);
}

function doDoubleLine($pdf, $w) {
  $wsum = 0;
  foreach ($w as $val) {
    $wsum += $val;
  }
  $pdf->Cell($wsum, 0.5, '', 1, 0, '', false, '', 0, true);
  $pdf->ln();
}

function doSolidLine($pdf, $widths, $height=0.5) {
  $wsum = 0;
  foreach ($widths as $val) {
    $wsum += $val;
  }
  $pdf->SetFillColor(0,0,0);
  $pdf->Cell($wsum, $height, '', 1, 0, '', true, '', 0, true);
  $pdf->SetFillColor(240, 240, 240);
  $pdf->ln();
}

function generatePDF($samples, $patient, $testtypes, $layout, $doctor, $laboadmin) {
  $pages = ceil(count($samples)/8);

  if($pages==0) {
    echo 'no pages to display';
    die();
  }

  $firstDate = null;
  $lastDate = null;

  $pdf = new \TCPDF();
  $pdf->SetFillColor(240, 240, 240);
  $pdf->SetPrintHeader(false); // custom header/footer rendering ... todo: change?
  $pdf->SetPrintFooter(false);
  //$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
  //$pdf->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
  //$pdf->SetFont('DejaVu','',12);

  for ($page = 0; $page < $pages; $page++) {
    $hasAnyContent = false;
    $hasContent = array(false);
    $subPage = 0;
    foreach ($layout as $block) {
      if(isset($block->type) && $block->type=="pagebreak") {
        $subPage++;
        $hasContent[$subPage] = false;
      } elseif(isset($block->type) && $block->type=="table") {
        if(isset($block->elements)) {
          foreach ($block->elements as $element) {
            if($hasContent[$subPage]) {
              break;
            }
            if(isset($element->id)) {
              $testtype = null;
              if(isset($testtypes[$element->id])) {
                $testtype = $testtypes[$element->id];
              }
              if($testtype==null || $testtype->getType() != "calculated") {
                for ($i=0; $i < 8; $i++) {
                  if($hasContent[$subPage]) {
                    break;
                  }
                  if(isset($samples[$page*8+$i])) {
                    $sample = $samples[$page*8+$i];
                    $test = $sample->getTestByTypeId($element->id);
                    if($test!=null) {
                      if($test->getValue() != '') {
                        $hasAnyContent = true;
                        $hasContent[$subPage] = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    if(!$hasAnyContent) {
      continue;
    }

    $subPage = 0;
    if($hasContent[$subPage]) {
      $pdf->AddPage();
    }
    foreach ($layout as $block) {
      if(isset($block->type)) {
        if ($block->type == "pagebreak") {
          $subPage++;
          if($hasContent[$subPage]) {
            $pdf->AddPage();
          }
        }
        if(!$hasContent[$subPage]) {
          continue;
        }
        if($block->type == "header") {
          printHeader($pdf, $doctor, $patient, $laboadmin->config()->get('general.dateformat.text', 'Y-m-d'));
        } elseif ($block->type =="table") {
          $fill = false;

          $width = array(56, 17, 17, 17, 17, 17, 17, 17, 17);
          $border = array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
          $align = array("C", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R");
          $fsize = array(12, 8, 8, 8, 8, 8, 8, 8, 8, 8);
          $fstyle = array('B', '', '', '', '', '', '', '', '', '');

          if(isset($block->title)) {
            $pdf->setFont('','UB',14);
            $pdf->Write(8, $block->title);
            $pdf->setFont('','',12);
            $pdf->Ln();
          }

          if(!isset($block->hideDates) || $block->hideDates!=true) {
            $dates = array('','','','','','','','');
            for ($i=0; $i < 8; $i++) {
              if(isset($samples[$page*8+$i])) {
                $date = $samples[$page*8+$i]->getDate();
                if(isset($date)) {
                  if($firstDate==null) $firstDate = $date;
                  $lastDate = $date;
                  $dates[$i]= $date->format($laboadmin->config()->get('general.dateformat.list', 'Y-m-d'));
                }
              }
            }
  
            $dates = array(_("Date"), $dates[0], $dates[1], $dates[2], $dates[3], $dates[4], $dates[5], $dates[6], $dates[7]);
  
            printRowToPDF($pdf, $dates, $width, $border, $align, $fsize, $fstyle, $fill);
          } else {
            doSolidLine($pdf, $width, 0.01);
          }

          $fill = false;

          if(isset($block->elements)) {
            $width = array(30, 26, 17, 17, 17, 17, 17, 17, 17, 17);
            foreach ($block->elements as $element) {
              $testtype = null;
              if(isset($element->id) && isset($testtypes[$element->id])) {
                $testtype = $testtypes[$element->id];
              } else {
                $testtype = null;
              }
              $border = array("L", "R", "LR", "LR", "LR", "LR", "LR", "LR", "LR", "LR", "LR");
              $width = array(30, 26, 17, 17, 17, 17, 17, 17, 17, 17);
              $align = array("L", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R");
              $fsize = array(12, 8, 12, 12, 12, 12, 12, 12, 12, 12, 12);

              $bstyle = 'solid';
              $bwidth = 0;
              if(isset($element->borderBottomStyle) || isset($element->borderBottomWidth)) {
                if(isset($element->borderBottomStyle)) {
                  if($element->borderBottomStyle=='double') {
                    $bstyle='double';
                  }
                }
                if(isset($element->borderBottomWidth)) {
                  $bwidth = $element->borderBottomWidth;
                }
              }

              if($bstyle=='solid' && $bwidth==1) {
                $border = array("LB", "RB", "LRB", "LRB", "LRB", "LRB", "LRB", "LRB", "LRB", "LRB", "LRB");
              }

              $fstyle = array('', '', '', '', '', '', '', '', '', '', '');
              
              if(isset($element->fontStyle)) {
                if($element->fontStyle == 'bold') {
                  $fstyle[0] = 'B';
                }
              }

              if(isset($element->type) && ($element->type == "spacer" || $element->type == "label")) {
                $elementtext = "";
                if(isset($element->text)) {
                  $elementtext = $element->text;
                }
                $data = array($elementtext, "", "", "", "", "", "", "", "", "");

                printRowToPDF($pdf, $data, $width, $border, $align, $fsize, $fstyle, $fill);
              } else {
                $results = array("","","","","","","","");
                if(isset($element->id)) {
                  for ($i=0; $i < 8; $i++) { 
                    if(isset($samples[$page*8+$i])) {
                      $sample = $samples[$page*8+$i];
                      $test = $sample->getTestByTypeId($element->id);
                      if($test!=null) {
                        $results[$i] = $test->getValue();
                      }
                    }
                  }
                }
                if(isset($testtype) && $testtype!=null && $testtype->getType() == "calculated") {
                  for ($i=0; $i < 8; $i++) { 
                    if(isset($samples[$page*8+$i])) {
                      $values = $samples[$page*8+$i]->getTestValuesForArray($testtype->getDepends());
                      if(count($testtype->getDepends()) == count($values)) {
                        try{
                          $results[$i] = Parser::solveIF($testtype->getFormula(), $values);
                          if(is_numeric($results[$i]) && is_numeric($testtype->getRound())) {
                            $results[$i] = round($results[$i], $testtype->getRound());
                          }
                        } catch (\Exception $e) {
                          $errorCode = $e->getCode();
                          switch ($errorCode) {
                            case 5501:
                              $results[$i] = _("Div 0");
                              break;
                            default:
                              break;
                          }
                        }
                      }
                    }
                  }
                }
                $name = "";
                $normals = "";
                if(isset($testtype) && $testtype!=null) {
                  $name = $testtype->getName();
                  $normals = $testtype->getNormalsTextFor($patient);
                  $unit = "";
                  $unit .= $testtype->getUnit();
                  if($unit!="") {
                    $normals .= " ".$unit;
                  }
                } else if(isset($element->id)) {
                  $name = $element->id;
                }
                $data = array($name, $normals, $results[0], $results[1], $results[2], $results[3], $results[4], $results[5], $results[6], $results[7]);

                printRowToPDF($pdf, $data, $width, $border, $align, $fsize, $fstyle, $fill);
              }
              
              if($bstyle=='double') {
                doDoubleLine($pdf, $width);
              }
              if($bstyle=='solid' && $bwidth>1) {
                doSolidLine($pdf, $width);
              }
            }
            doSolidLine($pdf, $width, 0.01);
          }
        }
      }
    }
  }
  $filenameFormat = $laboadmin->config()->get('general.pdf.filenameformat', '<CT> <patient> <pagename>');
  $dateformatfile = $laboadmin->config()->get('general.dateformat.file', 'Y-m-d');
  $ct = date($dateformatfile);
  $ft = '';
  if($firstDate!= null) {
    $ft = $firstDate->format($dateformatfile);
  }

  $lt = '';
  if($firstDate!= null) {
    $lt = $lastDate->format($dateformatfile);
  }
  $filename = str_replace('<CT>', $ct, $filenameFormat);
  $filename = str_replace('<FT>', $ft, $filename);
  $filename = str_replace('<LT>', $lt, $filename);
  $filename = str_replace('<pagename>', _("Datasheet"), $filename);
  $filename = str_replace('<patient>', $patient->getName(), $filename);

  $pdf->Output($filename.'.pdf', 'I');
}
?>